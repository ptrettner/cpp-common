#pragma once

#include <iostream>

#include <functional>
#include <string>
#include <sstream>
#include <limits>

#include "property.hh"

namespace debug
{
class verbose
{
public:
    using LogFunc = std::function<void(std::string const& msg, uint64_t flags)>;

    struct logObject
    {
        uint64_t flags;
        mutable std::stringstream ss;
        verbose const* parent;

        logObject(logObject&& rhs)
        {
            flags = rhs.flags;
            ss.str(rhs.ss.str());
            parent = rhs.parent;
            rhs.parent = nullptr;
        }

        logObject(uint64_t flags, verbose const* parent) : flags(flags), parent(parent) {}
        ~logObject()
        {
            if (parent && parent->mLogFunc)
                parent->mLogFunc(ss.str(), flags);
        }
    };

private:
    LogFunc mLogFunc;
    uint64_t mMask;

public:
    PROPERTY(Mask);

    friend struct logObject;

public:
    explicit verbose(LogFunc logFunc, uint64_t mask = std::numeric_limits<uint64_t>::max())
      : mLogFunc(logFunc), mMask(mask)
    {
    }

    bool is(uint64_t flags) const { return flags & mMask; }
    logObject operator()(uint64_t flags = std::numeric_limits<uint64_t>::max()) const
    {
        return logObject(flags, flags & mMask ? this : nullptr);
    }
    void operator=(uint64_t mask) { mMask = mask; }
    bool operator&(uint64_t flags) const { return is(flags); }
};

template <typename T>
verbose::logObject const& operator<<(verbose::logObject const& lo, T const& obj)
{
    if (lo.parent)
        lo.ss << obj;
    return lo;
}
}
