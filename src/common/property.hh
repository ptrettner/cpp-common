#pragma once

#include "macro_join.hh"

/** GETTER(name) SETTER(name) PROPERTY(name)
 * as non-typed getter/setter macros
 *
 * Usage:
 * class Foo {
 * private:
 *    int mBar;
 *    bool mFinished;
 *
 * public:
 *    PROPERTY(Bar); // generates getBar() and setBar(...) with const& set and get
 *    PROPERTY_IS(Finished); // isFinished() and setFinished()
 * };
 *
 * CAUTION: these macros can only be used _after_ the member is declared (due to type deduction)
 */

#define GETTER(name)                                                      \
    auto get##name() const->decltype(m##name) const & { return m##name; } \
    friend class MACRO_JOIN(___get_, __COUNTER__)

#define SETTER(name)                                                    \
    void set##name(decltype(m##name) const& value) { m##name = value; } \
    friend class MACRO_JOIN(___set_, __COUNTER__)

#define GETTER_IS(name)                                                  \
    auto is##name() const->decltype(m##name) const & { return m##name; } \
    friend class MACRO_JOIN(___get_is_, __COUNTER__)

#define PROPERTY(name) \
    GETTER(name);      \
    SETTER(name)

#define PROPERTY_IS(name) \
    GETTER_IS(name);      \
    SETTER(name)

/**
 * Qt specific addition
 *
 * Usage:
 *
 *  Q_OBJECT
 *  Q_PROPERTY(QString bar READ getBar WRITE setBar NOTIFY onBarChanged)
 * private:
 *  QString mBar;
 * public:
 *  PROPERTY_QML(Bar);
 * signals:
 *  void onBarChanged();
 */

#define SETTER_QML(name)                           \
    void set##name(decltype(m##name) const& value) \
    {                                              \
        if (m##name != value)                      \
        {                                          \
            m##name = value;                       \
            emit on##name##Changed();              \
        }                                          \
    }                                              \
    friend class MACRO_JOIN(___set_, __COUNTER__)

#define PROPERTY_QML(name) \
    GETTER(name);          \
    SETTER_QML(name)

#define PROPERTY_QML_IS(name) \
    GETTER_IS(name);          \
    SETTER_QML(name)


#define PROPERTY_QML_FULL(type, name)                                                \
    Q_PROPERTY(type m##name READ get##name WRITE set##name NOTIFY on##name##Changed) \
    Q_SIGNAL void on##name##Changed();                                               \
    GETTER(name);                                                                    \
    SETTER_QML(name)

#define PROPERTY_QML_IS_FULL(type, name)                                             \
    Q_PROPERTY(type m##name READ get##name WRITE set##name NOTIFY on##name##Changed) \
    Q_SIGNAL void on##name##Changed();                                               \
    GETTER_IS(name);                                                                 \
    SETTER_QML(name)
