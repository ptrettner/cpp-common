#pragma once

#include <glm/glm.hpp>

#include <cstdint>
#include <random>
#include <string>
#include <vector>

#include "shared.hh"

#include "random_generators.hh"

template <class Rng>
float uniform(Rng& rng, float min, float maxInclusive)
{
    float rmin = rng.min();
    float rmax = rng.max();
    return ((float)rng() - rmin) / (rmax - rmin) * (maxInclusive - min) + min;
}
template <class Rng>
double uniform(Rng& rng, double min, double maxInclusive)
{
    double rmin = rng.min();
    double rmax = rng.max();
    return ((double)rng() - rmin) / (rmax - rmin) * (maxInclusive - min) + min;
}
template <class Rng>
int uniform(Rng& rng, int min, int maxInclusive)
{
    return std::uniform_int_distribution<int>(min, maxInclusive)(rng);
}
template <class Vec2 = glm::vec2, class Rng>
Vec2 uniform_dir2(Rng& rng)
{
    double x, y;
    do
    {
        x = uniform(rng, -1.0, 1.0);
        y = uniform(rng, -1.0, 1.0);
    } while (x * x + y * y > 1);

    return normalize(Vec2(x, y));
}
template <class Vec3 = glm::vec3, class Rng>
Vec3 uniform_dir3(Rng& rng)
{
    double x, y, z;
    do
    {
        x = uniform(rng, -1.0, 1.0);
        y = uniform(rng, -1.0, 1.0);
        z = uniform(rng, -1.0, 1.0);
    } while (x * x + y * y + z * z > 1);

    return normalize(Vec3(x, y, z));
}

SHARED(class, Random);
class Random
{
private:
    class MersenneTwister
    {
    private:
        uint32_t mState[624];
        uint32_t mIndex = 0;

        void generateNumbers()
        {
            for (uint32_t i = 0; i < 624; i++)
            {
                uint32_t y = (mState[i] & 0x80000000) + (mState[(i + 1) % 624] & 0x7fffffff);
                mState[i] = mState[(i + 397) % 624] ^ (y >> 1);
                if (y % 2 != 0)
                    mState[i] = mState[i] ^ 0x9908b0df;
            }
        }

    public:
        explicit MersenneTwister(uint32_t _seed)
        {
            mIndex = 0;
            mState[0] = _seed;
            for (uint32_t i = 1; i < 624; ++i)
            {
                mState[i] = 0x6c078965ul * ((mState[i - 1] ^ (mState[i - 1] >> 30)) + i);
            }
        }

        uint32_t operator()()
        {
            if (mIndex == 0)
                generateNumbers();

            uint32_t y = mState[mIndex];
            y = y ^ (y >> 11);
            y = y ^ ((y << 7) & 0x9d2c5680);
            y = y ^ ((y << 15) & 0xefc60000);
            y = y ^ (y >> 18);

            mIndex = (mIndex + 1) % 624;
            return y;
        }
    };

    /// seed
    uint32_t mSeed;
    /// Random Number Generator
    MersenneTwister mMT;
    /// true, iff global random generator
    bool mGlobal;

    /// ctor
    Random(uint32_t _seed, bool _global);

public:
    /// true, iff global random generator
    bool isGlobal() const { return mGlobal; }
    /// gets the seed
    uint32_t getSeed() const { return mSeed; }
    /// ctor
    Random();
    /// ctor
    explicit Random(uint32_t seed);
    /// ctor
    explicit Random(const std::string& seed);
    /// dtor
    ~Random(void);

    /// no copy
    Random(Random const&) = delete;
    Random(Random&&) = delete;
    Random& operator=(Random const&) = delete;

    /// Gets a global (but thread-local) Random instance
    static Random* global();

    /// creates another random generator dependent of the seed of this generator
    std::unique_ptr<Random> getSubgenerator(uint32_t seed);
    /// creates another random generator dependent of the seed of this generator
    std::unique_ptr<Random> getSubgenerator(const std::string& seed);

    /// Returns an integer uniformly distributed between 0 and 2^32-1
    int uniformInt();

    /// Returns an integer uniformly distributed between min and max (including min AND max)
    int uniformInt(int _inclusiveMin, int _inclusiveMax);

    /// Returns a double uniformly distributed in [0,1)
    double uniformDouble();

    /// Returns true with the given probability
    bool coinFlip(double trueProb = .5);

    /// Returns a double uniformly distributed in [min,max)
    double uniformDouble(double _min, double _max);

    /// Returns a float uniformly distributed in [min,max)
    float uniformFloat(float _min, float _max);

    /// Returns a vec2 with components uniformly distributed in [min,max)
    glm::vec2 uniformVec2(glm::vec2 _min = glm::vec2(0), glm::vec2 _max = glm::vec2(1));

    /// Returns a vec3 with components uniformly distributed in [min,max)
    glm::vec3 uniformVec3(glm::vec3 _min = glm::vec3(0), glm::vec3 _max = glm::vec3(1));

    /// Returns a vec4 with components uniformly distributed in [min,max)
    glm::vec4 uniformVec4(glm::vec4 _min = glm::vec4(0), glm::vec4 _max = glm::vec4(1));

    /// Returns a standard normal distributed double
    double normalDouble();

    /// Returns a normal distributed double
    double normalDouble(double _mean, double _deviation);

    /// Returns a Poisson distributed integer
    unsigned int poissonInt(double _lambda);

    /// Returns an exponentially distributed double
    double exponentialDouble(double _lambda);

    /// Returns a normalized vec2 pointing to a random direction
    glm::vec2 randomDirection2();

    /// Returns a normalized vec3 pointing to a random direction
    glm::vec3 randomDirection3();

    /// Returns a random barycentric coordinate set (x + y + z = 1, x,y,z >= 0)
    glm::vec3 randomBarycentric();

    /// Returns a normalized vec3 pointing to a random direction
    glm::vec3 randomHemisphereDirection(const glm::vec3& _normal);

    /// returns a uniformly distributed random element of the given vector
    template <typename T>
    T const& randomElement(std::vector<T> const& v)
    {
        return v[uniformInt(0, v.size() - 1)];
    }

    /// Shuffles the values between the two iterators
    template <class RandomIt>
    void shuffle(RandomIt _begin, RandomIt _end)
    {
        typename std::iterator_traits<RandomIt>::difference_type i, n;
        n = _end - _begin;
        for (i = n - 1; i > 0; --i)
        {
            using std::swap;
            swap(_begin[i], _begin[uniformInt(0, n - 1)]);
        }
    }
};
