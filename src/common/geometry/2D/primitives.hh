#pragma once

#include <glm/glm.hpp>

namespace geometry_2d
{
struct line
{
    glm::vec2 pos;
    glm::vec2 dir; ///< not normalized
};

struct ray
{
    glm::vec2 pos;
    glm::vec2 dir; ///< not normalized

    line to_line() const { return {pos, dir}; }
};

struct segment
{
    glm::vec2 start;
    glm::vec2 end;

    line to_line() const { return {start, end - start}; }
};
}
