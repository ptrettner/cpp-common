#pragma once

#include <glm/glm.hpp>

#include "primitives.hh"

namespace geometry_2d
{
/// General line-line case that returns parameters t0, t1
/// intersection is l0.pos + l0.dir * t0 (or same with t1)
/// https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
bool intersect(line l0, line l1, float& t0, float& t1);

// specialized cases
bool intersect(line l0, line l1, glm::vec2* intersection = nullptr);
bool intersect(line l0, segment s1, glm::vec2* intersection = nullptr);
bool intersect(line l0, ray r1, glm::vec2* intersection = nullptr);
bool intersect(segment s0, line l1, glm::vec2* intersection = nullptr);
bool intersect(segment s0, segment s1, glm::vec2* intersection = nullptr);
bool intersect(segment s0, ray r1, glm::vec2* intersection = nullptr);
bool intersect(ray r0, line l1, glm::vec2* intersection = nullptr);
bool intersect(ray r0, segment s1, glm::vec2* intersection = nullptr);
bool intersect(ray r0, ray r1, glm::vec2* intersection = nullptr);

/// =============== IMPLEMENTATION ===============

inline bool intersect(line l0, line l1, float& t0, float& t1)
{
    auto x1 = l0.pos.x;
    auto y1 = l0.pos.y;

    auto x2 = l0.pos.x + l0.dir.x;
    auto y2 = l0.pos.y + l0.dir.y;

    auto x3 = l1.pos.x;
    auto y3 = l1.pos.y;

    auto x4 = l1.pos.x + l1.dir.x;
    auto y4 = l1.pos.y + l1.dir.y;

    auto denom = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

    if (denom == 0)
        return false;

    auto nom_t = (x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4);
    auto nom_u = (x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3);

    t0 = nom_t / denom;
    t1 = nom_u / denom;

    return true;
}

inline bool intersect(line l0, line l1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(l0, l1, t0, t1);

    if (k && intersection)
        *intersection = l0.pos + t0 * l0.dir;

    return k;
}
inline bool intersect(line l0, segment s1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(l0, s1.to_line(), t0, t1);

    if (k && intersection)
        *intersection = l0.pos + t0 * l0.dir;

    return k;
}
inline bool intersect(line l0, ray r1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(l0, r1.to_line(), t0, t1);

    if (k && intersection)
        *intersection = l0.pos + t0 * l0.dir;

    return k;
}
inline bool intersect(segment s0, line l1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(s0.to_line(), l1, t0, t1);

    if (k && intersection)
        *intersection = s0.start + t0 * (s0.end - s0.start);

    return k;
}
inline bool intersect(segment s0, segment s1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(s0.to_line(), s1.to_line(), t0, t1);

    if (k && intersection)
        *intersection = s0.start + t0 * (s0.end - s0.start);

    return k;
}
inline bool intersect(segment s0, ray r1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(s0.to_line(), r1.to_line(), t0, t1);

    if (k && intersection)
        *intersection = s0.start + t0 * (s0.end - s0.start);

    return k;
}
inline bool intersect(ray r0, line l1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(r0.to_line(), l1, t0, t1);

    if (k && intersection)
        *intersection = r0.pos + t0 * r0.dir;

    return k;
}
inline bool intersect(ray r0, segment s1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(r0.to_line(), s1.to_line(), t0, t1);

    if (k && intersection)
        *intersection = r0.pos + t0 * r0.dir;

    return k;
}
inline bool intersect(ray r0, ray r1, glm::vec2* intersection)
{
    float t0, t1;
    auto k = intersect(r0.to_line(), r1.to_line(), t0, t1);

    if (k && intersection)
        *intersection = r0.pos + t0 * r0.dir;

    return k;
}
}
