#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <functional>

#include <common/property.hh>

template <typename DataT>
class miniDB
{
public:
    struct Item
    {
        std::string name;
        std::function<void(std::ostream& ss, DataT const& datum)> mSaveFunc;
        std::function<void(std::istream& ss, DataT& datum)> mLoadFunc;

        template <typename MemberT>
        Item(std::string const& name, MemberT DataT::*memberPtr)
          : name(name)
        {
            mSaveFunc = [=](std::ostream& ss, DataT const& datum)
            {
                ss << datum.*memberPtr;
            };
            mLoadFunc = [=](std::istream& ss, DataT& datum)
            {
                ss >> datum.*memberPtr;
            };
        }
    };

private:
    std::string mFilename;
    std::vector<DataT> mData;
    std::vector<Item> mItems;
    bool mDirty = false;

public:
    GETTER(Data);

public:
    void load()
    {
        mData.clear();
        std::ifstream file(mFilename);
        if (file.good())
        {
            auto isHeader = true;
            for (std::string line; std::getline(file, line);)
            {
                if (isHeader)
                {
                    isHeader = false;
                    continue;
                }

                std::stringstream ss(line);
                auto idx = 0u;
                DataT datum;
                for (std::string val; std::getline(ss, val, ',');)
                {
                    if (idx >= mItems.size())
                    {
                        std::cerr << "More items than registered. Starting at: " << val << std::endl;
                        break;
                    }

                    // std::cout << "reading " << val << std::endl;

                    auto const& item = mItems[idx];
                    std::stringstream vss(val);
                    item.mLoadFunc(vss, datum);

                    ++idx;
                }

                mData.push_back(datum);
            }
        }
        mDirty = false;
    }
    void save()
    {
        if (mItems.size() == 0)
            return;

        std::ofstream file(mFilename);
        for (auto i = 0u; i < mItems.size(); ++i)
            file << (i == 0 ? "" : ",") << mItems[i].name;
        file << "\n";
        for (auto const& datum : mData)
        {
            for (auto i = 0u; i < mItems.size(); ++i)
            {
                if (i > 0)
                    file << ",";
                mItems[i].mSaveFunc(file, datum);
            }
            file << "\n";
        }
        mDirty = false;
    }
    ~miniDB()
    {
        if (mDirty)
            save();
    }

    miniDB(std::string const& filename, std::vector<Item> const& items) : mFilename(filename), mItems(items) { load(); }
    miniDB& operator<<(DataT const& value)
    {
        mData.push_back(value);
        mDirty = true;
        return *this;
    }
};
