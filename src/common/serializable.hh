#pragma once

#include <common/json.hh>

#include <glm/glm.hpp>

#include <iostream>

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <type_traits>
#include <vector>

/**
 * Usage:
 *
 * // alternatively: SERIALIZABLE(MySettings)
 * struct MyObject : serializables::object<MySettings>
 * {
 *   MEMBER(float, Foo1) = 17;
 *   MEMBER(bool, Foo2) = false;
 *   MEMBER(std::string, Foo3);
 *   MEMBER(float, Foo4);
 *
 *   FIELD(float, bar) = 3.14f;
 * };
 *
 * MySettings s;
 * s.bar = 19.6f;
 * s.setFoo1(3);
 * some_json = s;
 * some_json = s.to_json();
 * s.from_json(some_json_value);
 */

#define MEMBER(Type, Name)                                                                                  \
private:                                                                                                    \
    struct _init_##Name                                                                                     \
    {                                                                                                       \
        _init_##Name() { static property_adder pa(#Name, serializables::make_property(&this_t::m##Name)); } \
    } _init_##Name;                                                                                         \
                                                                                                            \
public:                                                                                                     \
    Type& get##Name() { return m##Name; }                                                                   \
    Type const& get##Name() const { return m##Name; }                                                       \
    void set##Name(Type const& v) { m##Name = v; }                                                          \
                                                                                                            \
private:                                                                                                    \
    Type m##Name

#define FIELD(Type, Name)                                                                                \
private:                                                                                                 \
    struct _init_##Name                                                                                  \
    {                                                                                                    \
        _init_##Name() { static property_adder pa(#Name, serializables::make_property(&this_t::Name)); } \
    } _init_##Name;                                                                                      \
                                                                                                         \
public:                                                                                                  \
    Type Name

#define SERIALIZABLE(Name) struct Name : serializables::object<Name>

#define JSON_SERIALIZER(Name)                                   \
    inline void to_json(nlohmann::json& j, Name const& obj)     \
    {                                                           \
        for (auto const& kvp : Name::properties())              \
            j[kvp.first] = kvp.second->serialize(&obj);         \
    }                                                           \
    inline void from_json(nlohmann::json const& j, Name& obj)   \
    {                                                           \
        obj = {};                                               \
        for (auto const& kvp : Name::properties())              \
            if (j.count(kvp.first))                             \
                kvp.second->deserialize(&obj, j.at(kvp.first)); \
    }


namespace serializables
{
using json = nlohmann::json;

template <class ThisT>
struct object;
struct object_base;

template <class T, class Enabled = typename std::enable_if<!std::is_assignable<object_base&, T>::value>::type>
void assign_json(T& value, json const& j)
{
    value = {};
    j.get_to<T>(value);
}
inline void assign_json(json& value, json const& j)
{
    value = j; // copy
}
template <class T>
void assign_json(object<T>& value, json const& j)
{
    value.from_json(j);
}
template <class T>
void assign_json(glm::tvec2<T>& value, json const& j)
{
    if (j.is_array() && j.size() == 2)
    {
        if (j[0].is_number())
            value.x = j[0];
        if (j[1].is_number())
            value.y = j[1];
    }
    else
        value = {0, 0};
}
template <class T>
void assign_json(glm::tvec3<T>& value, json const& j)
{
    if (j.is_array() && j.size() == 3)
    {
        if (j[0].is_number())
            value.x = j[0];
        if (j[1].is_number())
            value.y = j[1];
        if (j[2].is_number())
            value.z = j[2];
    }
    else
        value = {0, 0, 0};
}
template <class T>
void assign_json(glm::tvec4<T>& value, json const& j)
{
    if (j.is_array() && j.size() == 4)
    {
        if (j[0].is_number())
            value.x = j[0];
        if (j[1].is_number())
            value.y = j[1];
        if (j[2].is_number())
            value.z = j[2];
        if (j[3].is_number())
            value.w = j[3];
    }
    else
        value = {0, 0, 0, 0};
}

template <class T>
json create_json(T const& value)
{
    return value;
}
template <class T>
json create_json(glm::tvec2<T> const& value)
{
    return json::array({value.x, value.y});
}
template <class T>
json create_json(glm::tvec3<T> const& value)
{
    return json::array({value.x, value.y, value.z});
}
template <class T>
json create_json(glm::tvec4<T> const& value)
{
    return json::array({value.x, value.y, value.z, value.w});
}

template <class ObjT>
struct property_base
{
    virtual void deserialize(ObjT* obj, json const& j) = 0;
    virtual json serialize(ObjT const* obj) const = 0;
    virtual ~property_base() {}
};

template <class T, class ObjT>
struct property : property_base<ObjT>
{
    T ObjT::*value;
    property(T ObjT::*value) : value(value) {}
    virtual void deserialize(ObjT* obj, json const& j) override { assign_json(obj->*value, j); }
    virtual json serialize(ObjT const* obj) const override { return create_json(obj->*value); }
};

template <class T, class ObjT>
property<T, ObjT>* make_property(T ObjT::*value)
{
    return new property<T, ObjT>(value);
}

struct object_base
{
};

template <class ThisT>
struct object : object_base
{
    using this_t = ThisT;

public:
    static std::map<std::string, std::unique_ptr<serializables::property_base<ThisT>>>& properties()
    {
        static std::map<std::string, std::unique_ptr<serializables::property_base<ThisT>>> sProperties;
        return sProperties;
    }

protected:
    struct property_adder
    {
        property_adder(char const* name, serializables::property_base<ThisT>* prop)
        {
            static std::mutex sMutex;
            sMutex.lock();
            assert(!properties().count(name) && "Property already registered");
            // std::cout << "registering " << name << " / " << (void*)prop << " at " << (void*)&sProperties << std::endl;
            properties()[name].reset(prop);
            sMutex.unlock();
        }
    };

public:
    json to_json() const
    {
        json j;
        for (auto const& p : properties())
            j[p.first] = p.second->serialize(static_cast<ThisT const*>(this));
        return j;
    }

    void from_json(json const& j)
    {
        for (auto const& p : properties())
            if (j.count(p.first))
                p.second->deserialize(static_cast<ThisT*>(this), j[p.first]);
    }

    // DOESN'T WORK: hidden in derived class
    // ThisT& operator=(json const& j)
    // {
    //    from_json(j);
    //    return static_cast<ThisT&>(*this);
    // }
    operator json() const { return to_json(); }
};

// template <class ThisT>
// std::map<std::string, std::unique_ptr<serializables::property_base<ThisT>>> object<ThisT>::sProperties;
// template <class ThisT>
// std::mutex object<ThisT>::sMutex;
}
