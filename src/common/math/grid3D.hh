#pragma once

#include <glm/glm.hpp>
#include <vector>
// #include <valarray>

#include <common/property.hh>

namespace math
{
template <typename T>
class grid3D
{
private:
    glm::uvec3 mCellCount;
    glm::vec3 mGridSize;
    size_t mCellsTotal;

    glm::vec3 mAabbMin;
    glm::vec3 mAabbMax;

    std::vector<T> mValues;

public:
    GETTER(AabbMin);
    GETTER(AabbMax);
    GETTER(CellCount);
    GETTER(GridSize);
    GETTER(Values);

public:
    size_t indexOf(glm::vec3 const& worldPos) const
    {
        auto ipos = (glm::ivec3)((worldPos - mAabbMin) / mGridSize);
        auto x = glm::clamp(ipos.x, 0, (int)mCellCount.x - 1);
        auto y = glm::clamp(ipos.y, 0, (int)mCellCount.y - 1);
        auto z = glm::clamp(ipos.z, 0, (int)mCellCount.z - 1);
        auto idx = (z * mCellCount.y + y) * mCellCount.x + x;
        assert(idx >= 0 && idx < mValues.size());
        return idx;
    }
    typename std::vector<T>::reference value(glm::vec3 const& worldPos) { return mValues[indexOf(worldPos)]; }
    typename std::vector<T>::const_reference value(glm::vec3 const& worldPos) const
    {
        return mValues[indexOf(worldPos)];
    }
    typename std::vector<T>::reference operator[](glm::vec3 const& worldPos) { return value(worldPos); }
    typename std::vector<T>::const_reference operator[](glm::vec3 const& worldPos) const { return value(worldPos); }
public:
    grid3D(glm::uvec3 cellCount, glm::vec3 aabbMin, glm::vec3 aabbMax, T const& defaultValue = T())
      : mCellCount(cellCount), mGridSize((aabbMax - aabbMin) / (glm::vec3)mCellCount), mAabbMin(aabbMin), mAabbMax(aabbMax)
    {
        mCellsTotal = mCellCount.x * mCellCount.y * mCellCount.z;
        mValues.resize(mCellsTotal, defaultValue);
    }
};
}
