#pragma once

#include <glm/glm.hpp>

#include "../simplex.hh"

namespace glm
{
namespace detail
{
uint32_t splitmix32_s(uint32_t state)
{
    uint32_t z = state + 0x9e3779b9;
    z ^= z >> 15; // 16 for murmur3
    z *= 0x85ebca6b;
    z ^= z >> 13;
    z *= 0xc2b2ae3d; // 0xc2b2ae35 for murmur3
    return z ^= z >> 16;
}

float uint_to_float(uint32_t hash)
{
    return float(hash) * 2.32830644e-10; // 1 / (2^32 - 1)
}

float splitmix32_sf(uint32_t state)
{
    return uint_to_float(splitmix32_s(state));
}

uint32_t combine2(uint32_t a, uint32_t b)
{
    return (a << 16) + (a >> 16) + b;
}

uint32_t combine2(glm::vec2 v)
{
    return combine2(glm::floatBitsToUint(v.x), glm::floatBitsToUint(v.y));
}
}

int fast_ifloor(float x)
{
    return x >= 0 ? (int)x : (int)x - 1;
}
ivec2 fast_ifloor(vec2 v)
{
    return {fast_ifloor(v.x), fast_ifloor(v.y)};
}

float sharpen(float x, float gain)
{
    if (x < 0.5)
        return 0.5 * pow(x * 2, gain);
    else
        return 1 - 0.5 * pow((1 - x) * 2, gain);
}

float terrace(float h, float s = 1.0f, float gain = 8.0f, float offset = 0.0f)
{
    h -= offset;
    h /= s;
    float hr = floor(h);
    float frac = h - hr;

    h = hr + sharpen(frac, gain);
    return h * s + offset;
}


float worley(glm::vec2 pos)
{
    using namespace detail;

    glm::ivec2 ip = fast_ifloor(pos);

    float min_dis = 1e30f;

    for (int dy = -1; dy <= 1; ++dy)
        for (int dx = -1; dx <= 1; ++dx)
        {
            int x = ip.x + dx;
            int y = ip.y + dy;
            uint32_t seed = combine2(glm::vec2(x, y));

            float sx = x + splitmix32_sf(seed);
            float sy = y + splitmix32_sf(seed + 1);

            float dis = distance2(glm::vec2(sx, sy), pos);
            min_dis = glm::min(min_dis, dis);
        }

    return min_dis;
}
}
