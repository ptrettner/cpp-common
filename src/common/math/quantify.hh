#pragma once

#include <glm/glm.hpp>

namespace glm
{
// 0.0 -> 0
// 1.0 -> 255
// (clamped)
inline uint8_t norm_float_to_u8_clamped(float f)
{
    return (uint8_t)glm::clamp(f * 256, 0.0f, 255.0f);
}

// 0   -> 0.0
// 255 -> 1.0
inline float u8_to_norm_float(uint8_t u)
{
    return u / 255.0f;
}
}
