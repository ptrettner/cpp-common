#pragma once

#include <vector>

namespace glm
{

inline std::vector<glm::vec3> transform(std::vector<glm::vec2> const& pts, glm::mat4 const& m)
{
    std::vector<glm::vec3> output(pts.size());
    for (auto i = 0u; i < pts.size(); ++i)
        output[i] = glm::vec3(m * glm::vec4(pts[i], 0.0f, 1.0f));
    return output;
}

}
