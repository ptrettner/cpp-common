#pragma once

#include <glm/glm.hpp>

namespace glm
{
/// calculates a orthonormal basis where the given dir is the x direction
inline glm::mat3 basisForX(glm::vec3 up)
{
    up = normalize(up);
    auto left = glm::abs(up.y) < glm::abs(up.z) ? glm::vec3(0, 1, 0) : glm::vec3(0, 0, 1);
    auto front = normalize(cross(left, up));
    left = cross(up, front);
    return {left, up, front};
}
/// calculates a orthonormal basis where the given dir is the y direction
inline glm::mat3 basisForY(glm::vec3 up)
{
    up = normalize(up);
    auto left = glm::abs(up.x) < glm::abs(up.z) ? glm::vec3(1, 0, 0) : glm::vec3(0, 0, 1);
    auto front = normalize(cross(left, up));
    left = cross(up, front);
    return {left, up, front};
}
/// calculates a orthonormal basis where the given dir is the z direction
inline glm::mat3 basisForZ(glm::vec3 up)
{
    up = normalize(up);
    auto left = glm::abs(up.x) < glm::abs(up.y) ? glm::vec3(1, 0, 0) : glm::vec3(0, 1, 0);
    auto front = normalize(cross(left, up));
    left = cross(up, front);
    return {left, up, front};
}
}
