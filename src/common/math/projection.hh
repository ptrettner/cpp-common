#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace glm
{
template <class T>
tvec3<T> unproject(tvec3<T> const& p, tmat4x4<T> const& inv_view, tmat4x4<T> const& inv_proj)
{
    auto v = inv_proj * tvec4<T>(p, 1);
    v /= v.w;
    return tvec3<T>(inv_view * v);
}
}
