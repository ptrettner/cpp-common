#pragma once

#include <cmath>

namespace glm
{
template <typename T>
float frobeniusNorm(T const& m)
{
    float sum = 0.f;
    for (auto i = 0; i < m.length(); ++i)
    {
        auto const& c = m[i];
        for (auto j = 0; j < c.length(); ++j)
        {
            auto v = c[j];
            sum += v * v;
        }
    }
    return sqrt(sum);
}
}
