#pragma once

#include <string>

#include <glm/gtx/color_space.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include "bitfield_reverse.hh"

namespace glm
{
// format: RRGGBB (no 0x!)
inline glm::vec3 hexToRgb(std::string const& h)
{
    int r, g, b;
#ifdef _MSC_VER
    sscanf_s(h.c_str(), "%02x%02x%02x", &r, &g, &b);
#else
    sscanf(h.c_str(), "%02x%02x%02x", &r, &g, &b);
#endif
    return glm::vec3(r, g, b) / 255.0f;
}

inline std::string rgbToHex(uint32_t rgb)
{
    static auto hexc = "0123456789ABCDEF";

    auto r = (rgb >> 16) & 0xFF;
    auto g = (rgb >> 8) & 0xFF;
    auto b = (rgb >> 0) & 0xFF;

    auto rh = r >> 4;
    auto rl = r & 0xF;
    auto gh = g >> 4;
    auto gl = g & 0xF;
    auto bh = b >> 4;
    auto bl = b & 0xF;

    std::string hex = "#000000";
    hex[1] = hexc[rh];
    hex[2] = hexc[rl];
    hex[3] = hexc[gh];
    hex[4] = hexc[gl];
    hex[5] = hexc[bh];
    hex[6] = hexc[bl];
    return hex;
}

inline glm::vec4 colorFromRgb(uint32_t rgb)
{
    return glm::vec4((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, (rgb >> 0) & 0xFF, (rgb >> 24) & 0xFF) / 255.9999f;
}
inline uint32_t colorToRgb(glm::vec4 const& v)
{
    uint32_t r = glm::clamp((int)(v.r * 255.9999f), 0, 255);
    uint32_t g = glm::clamp((int)(v.g * 255.9999f), 0, 255);
    uint32_t b = glm::clamp((int)(v.b * 255.9999f), 0, 255);
    uint32_t a = glm::clamp((int)(v.a * 255.9999f), 0, 255);
    return (r << 16u) + (g << 8u) + (b << 0u) + (a << 24u);
}
inline uint32_t colorToRgb(glm::vec3 const& v)
{
    uint32_t r = glm::clamp((int)(v.r * 255.9999f), 0, 255);
    uint32_t g = glm::clamp((int)(v.g * 255.9999f), 0, 255);
    uint32_t b = glm::clamp((int)(v.b * 255.9999f), 0, 255);
    return (r << 16u) + (g << 8u) + (b << 0u);
}

/// idx 0 = rwth blue in default
/// startHue is additive, changing it simply shifts the hue of all colors
inline glm::vec3 colorSeries(uint32_t idx, float startHue = 208.3f, float saturation = 1.0f, float value = 0.624f)
{
    auto hue = float(double(bitfieldReverse(idx)) / std::numeric_limits<uint32_t>::max() * 360.f + startHue);
    hue = fmodf(hue, 360.f);
    if (hue < 0.f)
        hue += 360.f;

    return glm::rgbColor(glm::vec3{hue, saturation, value});
}
}
