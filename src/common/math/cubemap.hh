#pragma once

#include <cassert>

#include <glm/glm.hpp>

namespace glm
{
/// x from -1..1
/// y from -1..1
/// layer from 0..5
inline vec3 cubemap_direction(float x, float y, uint layer)
{
    // see ogl spec 8.13. CUBE MAP TEXTURE SELECTION
    switch (layer)
    {
    // +x
    case 0:
        return vec3(+1, -y, -x);
    // -x
    case 1:
        return vec3(-1, -y, +x);
    // +y
    case 2:
        return vec3(+x, +1, +y);
    // -y
    case 3:
        return vec3(+x, -1, -y);
    // +z
    case 4:
        return vec3(+x, -y, +1);
    // -z
    case 5:
        return vec3(-x, -y, -1);
    }

    assert(0);
    return vec3(0, 1, 0);
}

/// given an unnormalized direction dir, returns layer and -1..1 xy for the lookup
inline std::pair<int, vec2> cubemap_lookup(vec3 dir)
{
    auto ad = abs(dir);
    auto maxd = max(ad.x, max(ad.y, ad.z));
    auto nd = dir / maxd;

    if (dir.x == maxd)
        return {0, {-nd.z, -nd.y}};
    if (dir.x == -maxd)
        return {1, {+nd.z, -nd.y}};

    if (dir.y == maxd)
        return {2, {+nd.x, +nd.z}};
    if (dir.y == -maxd)
        return {3, {+nd.x, -nd.z}};

    if (dir.z == maxd)
        return {4, {+nd.x, -nd.y}};
    if (dir.z == -maxd)
        return {5, {-nd.x, -nd.y}};

    assert(0);
    return {-1, {}};
}
}
