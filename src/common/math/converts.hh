#pragma once

#include <glm/vec4.hpp>
#include <glm/gtc/quaternion.hpp>

namespace glm
{
inline glm::vec4 quat_to_vec4(glm::quat const& q)
{
    return {q.x, q.y, q.z, q.w};
}
inline glm::quat vec4_to_quat(glm::vec4 const& v)
{
    return {v.w, v.x, v.y, v.z};
}
}
