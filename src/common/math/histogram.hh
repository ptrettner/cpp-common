#pragma once

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

namespace math
{
template <class T>
void print_histogram(std::vector<T> const& values, int bins = 16, int max_width = 60)
{
    auto mm = minmax_element(values.begin(), values.end());
    auto min = *mm.first;
    auto max = *mm.second;
    auto w = max - min + 1e-30f;

    std::vector<int> counts(bins, 0);
    for (auto const& v : values)
    {
        int bin = (v - min) / w * bins;
        if (bin >= bins)
            bin = bins - 1;

        counts[bin]++;
    }

    auto maxCnt = *max_element(counts.begin(), counts.end());

    for (auto i = 0; i < bins; ++i)
    {
        auto c_min = (i + 0.0f) * w + min;
        auto c_max = (i + 1.0f) * w + min;
        std::cout << std::setw(8) << c_min << " .." << std::setw(8) << c_max << ": ";
        int cnt = std::ceil(max_width * counts[i] / (maxCnt + 0.001f));
        std::cout << std::string(cnt, '#') << "\n";
    }
    std::cout << std::flush;
}
}
