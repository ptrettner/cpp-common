#pragma once

#include <cstddef>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>

#include "statstream.hh"

template <typename T = double>
class valuestream
{
private:
    statstream<T> stats;

    std::vector<T> mValues;
    mutable std::vector<T> mValuesSorted;
    mutable bool mSorted = false;

public:
    T average() const { return stats.average(); }
    size_t size() const { return stats.size(); }
    T variance() const { return stats.variance(); }
    T standardDev() const { return stats.standardDev(); }
    T min() const { return stats.min(); }
    T max() const { return stats.max(); }
    T median() const { return stats.size() == 0 ? T() : sorted()[stats.size() / 2]; }
    T percentile(float perc)
    {
        return stats.size() == 0 ? T() : sorted()[perc <= 0 ? 0 : perc >= 1 ? stats.size() - 1 : (int)(perc * stats.size())];
    }

public:
    std::vector<T> const& sorted() const
    {
        if (!mSorted)
        {
            mSorted = true;

            mValuesSorted = mValues;
            sort(begin(mValuesSorted), end(mValuesSorted));
        }

        return mValuesSorted;
    }

public:
    valuestream() { clear(); }
    void clear()
    {
        stats.clear();

        mSorted = false;
        mValues.clear();
        mValuesSorted.clear();
    }

    void push(std::initializer_list<T> const& vs)
    {
        for (auto const& v : vs)
            push(v);
    }
    void push(std::vector<T> const& vs)
    {
        for (auto const& v : vs)
            push(v);
    }
    void push(T const& v)
    {
        stats.push(v);
        mValues.push_back(v);

        mSorted = false;
    }

    valuestream& operator<<(T const& v)
    {
        push(v);
        return *this;
    }
    valuestream& operator<<(std::initializer_list<T> const& vs)
    {
        push(vs);
        return *this;
    }
    valuestream& operator<<(std::vector<T> const& vs)
    {
        push(vs);
        return *this;
    }

    void writeTableCSV(std::string const& filename, int columns = 1) const
    {
        std::ofstream file(filename);
        writeTableCSV(file, columns);
    }
    void writeTableCSV(std::string const& filename, std::vector<std::string> const& header) const
    {
        std::ofstream file(filename);
        auto hi = 0;
        for (auto const& h : header)
            file << (hi++ > 0 ? "," : "") << h;
        file << "\n";
        writeTableCSV(file, header.size());
    }
    void writeTableCSV(std::ostream& oss, int columns = 1) const
    {
        if (columns <= 0)
            columns = 1;

        for (auto i = 0u; i < mValues.size(); ++i)
        {
            oss << mValues[i];
            if (i % columns == columns - 1)
                oss << "\n";
            else
                oss << ",";
        }
    }
};
