#pragma once

#include <cstdint>

inline uint32_t bitfieldReverse(uint32_t x)
{
    int res = 0;
    int shift, mask;

    for (int i = 0; i < 32; i++)
    {
        mask = 1 << i;
        shift = 32 - 2 * i - 1;
        mask &= x;
        mask = (shift > 0) ? mask << shift : mask >> -shift;
        res |= mask;
    }

    return res;
}
