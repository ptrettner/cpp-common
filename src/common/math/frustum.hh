#pragma once

#include <array>

#include <glm/glm.hpp>

#include "aabb.hh"
#include "projection.hh"

namespace glm
{
template <class Scalar>
struct view_frustum
{
    using vec3 = tvec3<Scalar>;
    using vec4 = tvec4<Scalar>;
    using mat4 = tmat4x4<Scalar>;

    vec3 v;
    std::array<vec3, 4> corners;

    vec3 aabb_min;
    vec3 aabb_max;

    struct plane
    {
        vec3 n;
        Scalar dis;

        bool is_outside(vec3 p) const { return dot(p, n) > dis; }
    };
    std::array<plane, 4> planes;

    view_frustum() = default;

    view_frustum(vec3 const& cam_pos, mat4 const& inv_view, mat4 const& inv_proj)
    {
        v = cam_pos;

        corners[0] = unproject(vec3(-1, -1, 1), inv_view, inv_proj);
        corners[1] = unproject(vec3(+1, -1, 1), inv_view, inv_proj);
        corners[2] = unproject(vec3(+1, +1, 1), inv_view, inv_proj);
        corners[3] = unproject(vec3(-1, +1, 1), inv_view, inv_proj);

        // precompute AABB
        aabb_min = v;
        aabb_max = v;

        for (auto const& v : corners)
        {
            aabb_min = min(aabb_min, v);
            aabb_max = max(aabb_max, v);
        }

        // precompute planes
        for (auto i = 0; i < 4; ++i)
        {
            auto v0 = v;
            auto v1 = corners[i];
            auto v2 = corners[(i + 1) % 4];

            auto n = normalize(cross(v1 - v0, v2 - v0));
            auto d = dot(n, v0);

            // correct order
            assert(dot(n, (corners[0] + corners[1] + corners[2] + corners[3]) * Scalar(0.25)) < d);

            planes[i] = {n, d};
        }
    }
};

// outside means positive with normal
template <class Scalar>
bool is_outside_plane(view_frustum<Scalar> const& f, tvec3<Scalar> const& n, tvec3<Scalar> const& p)
{
    for (auto const& c : f.corners)
        if (dot(c - p, n) < 0)
            return false;

    return true;
}

template <class Scalar>
bool could_intersect_aabb(view_frustum<Scalar> const& f, tvec3<Scalar> const& amin, tvec3<Scalar> const& amax)
{
    // frustum against aabb
    if (!intersect_aabb(f.aabb_min, f.aabb_max, amin, amax))
        return false;

    // check frustum planes
    for (auto const& p : f.planes)
        if (is_aabb_outside_plane(p.n, p.dis, amin, amax))
            return false;

    // TODO: more planes?

    return true;
}
}
