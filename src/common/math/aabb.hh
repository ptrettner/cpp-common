#pragma once

#include <glm/glm.hpp>

namespace glm
{
template <class T>
bool is_in_aabb(tvec3<T> const& v, tvec3<T> const& amin, tvec3<T> const& amax)
{
    if (v.x < amin.x)
        return false;
    if (v.y < amin.y)
        return false;
    if (v.z < amin.z)
        return false;

    if (v.x > amax.x)
        return false;
    if (v.y > amax.y)
        return false;
    if (v.z > amax.z)
        return false;

    return true;
}

template <class T>
bool is_aabb_contained_in_aabb(tvec3<T> const& amin, tvec3<T> const& amax, tvec3<T> const& bmin, tvec3<T> const& bmax)
{
    return bmin.x <= amin.x && //
           bmin.y <= amin.y && //
           bmin.z <= amin.z && //
           amax.x <= bmax.x && //
           amax.y <= bmax.y && //
           amax.z <= bmax.z;
}

template <class T>
bool is_aabb_outside_plane(tvec3<T> const& normal, T dis, tvec3<T> const& amin, tvec3<T> const& amax)
{
    auto center = (amin + amax) * (1 / T(2));
    auto half_extent = amax - center;

    return dot(center, normal)                 //
               - abs(half_extent.x * normal.x) //
               - abs(half_extent.y * normal.y) //
               - abs(half_extent.z * normal.z)
           >= dis;
}

template <class T>
bool intersect_aabb(tvec3<T> const& amin, tvec3<T> const& amax, tvec3<T> const& bmin, tvec3<T> const& bmax)
{
    if (amax.x < bmin.x)
        return false;
    if (amax.y < bmin.y)
        return false;
    if (amax.z < bmin.z)
        return false;

    if (amin.x > bmax.x)
        return false;
    if (amin.y > bmax.y)
        return false;
    if (amin.z > bmax.z)
        return false;

    return true;
}

template <class T>
T distance_to_aabb(tvec3<T> const& v, tvec3<T> const& amin, tvec3<T> const& amax)
{
    return distance(v, clamp(v, amin, amax));
}
}
