#pragma once

#include <glm/glm.hpp>

namespace glm
{
template <int N, class T>
struct box
{
    static_assert(N == 3, "glm messed up internal types");
    using point = tvec3<T>;

    point min;
    point max;

    static box between(point a, point b) { return {a, b}; }
};

using box2 = box<2, float>;
using box2d = box<2, double>;
using box3 = box<3, float>;
using box3d = box<3, double>;

template <int N, class T>
box<N, T> merge(box<N, T> const& a, box<N, T> const& b)
{
    return {min(a.min, b.min), max(a.max, b.max)};
}

template <class T>
bool is_contained_in(tvec3<T> const& p, box<3, T> const& b)
{
    return b.min.x <= p.x && p.x <= b.max.x && //
           b.min.y <= p.y && p.y <= b.max.y && //
           b.min.z <= p.z && p.z <= b.max.z;
}

template <int N, class T>
bool intersects(box<N, T> const& a, box<N, T> const& b)
{
    if (a.max.x < b.min.x)
        return false;
    if (a.max.y < b.min.y)
        return false;
    if (a.max.z < b.min.z)
        return false;

    if (a.min.x > b.max.x)
        return false;
    if (a.min.y > b.max.y)
        return false;
    if (a.min.z > b.max.z)
        return false;

    return true;
}
}
