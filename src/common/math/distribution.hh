#pragma once

#include <vector>

#include <common/random.hh>
#include <common/property.hh>

template <typename T = int>
class distribution
{
private:
    std::vector<std::pair<T, double>> mValues;
    double mTotalWeight = 0.0;

public:
    GETTER(Values);
    GETTER(TotalWeight);

public:
    distribution(std::vector<std::pair<T, double>> values = {}) : mValues(values)
    {
        for (auto const& kvp : mValues)
            mTotalWeight += kvp.second;
    }

    distribution<T>& add(T val, double weight)
    {
        mTotalWeight += weight;
        mValues.push_back({val, weight});
        return *this;
    }

    void clear()
    {
        mValues.clear();
        mTotalWeight = 0.0;
    }

    T sample(Random& r) const
    {
        auto w = r.uniformDouble(0, mTotalWeight);
        for (auto const& kvp : mValues)
        {
            w -= kvp.second;
            if (w <= 0.0)
                return kvp.first;
        }
        return mValues[0].first;
    }

    double probOf(T val) const { return weightOf(val) / mTotalWeight; }
    double weightOf(T val) const
    {
        for (auto const& kvp : mValues)
            if (kvp.first == val)
                return kvp.second;
        return 0.0;
    }

    T operator()(Random& r) const { return sample(r); }
};
