#pragma once

#include <cstddef>
#include <vector>
#include <algorithm>

template <typename T = double>
class statstream
{
private:
    size_t mSize;
    T mSum;
    T mSumOfSquares;
    T mMin;
    T mMax;

public:
    T average() const { return mSize == 0 ? T() : mSum / mSize; }
    size_t size() const { return mSize; }
    T variance() const { return mSize == 0 ? T() : ((mSum / mSize) * mSum - mSumOfSquares) / mSize; }
    T standardDev() const { return sqrt(variance()); }
    T min() const { return mMin; }
    T max() const { return mMax; }
public:
    statstream() { clear(); }
    void clear()
    {
        mSize = 0;
        mSum = T();
        mSumOfSquares = T();
        mMin = T();
        mMax = T();
    }

    void push(std::initializer_list<T> const& vs)
    {
        for (auto const& v : vs)
            push(v);
    }
    void push(std::vector<T> const& vs)
    {
        for (auto const& v : vs)
            push(v);
    }
    void push(T const& v)
    {
        if (mSize == 0)
        {
            mMin = v;
            mMax = v;
        }
        else
        {
            if (v < mMin)
                mMin = v;
            if (v > mMax)
                mMax = v;
        }

        mSize += 1;
        mSum += v;
        mSumOfSquares += v * v;
    }

    statstream& operator<<(T const& v)
    {
        push(v);
        return *this;
    }
    statstream& operator<<(std::initializer_list<T> const& vs)
    {
        push(vs);
        return *this;
    }
    statstream& operator<<(std::vector<T> const& vs)
    {
        push(vs);
        return *this;
    }
};
