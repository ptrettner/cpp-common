#pragma once

#include <vector>

namespace ds
{
template <class T>
struct flat_stack
{
    void push(T const& t) { data.push_back(t); }
    void pop() { data.pop_back(); }

    T& top() { return data.back(); }
    T const& top() const { return data.back(); }

    void clear() { data.clear(); }
    bool empty() const { return data.empty(); }
    size_t size() const { return data.size(); }

private:
    std::vector<T> data;
};
}
