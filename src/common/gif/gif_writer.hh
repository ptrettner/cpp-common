#pragma once

#include "gif-h.hh"

#include <cassert>
#include <string>
#include <vector>

// NOTE: only RGBA8 input supported! (alpha ignored)
struct gif_writer
{
private:
    gif_h::GifWriter writer;
    int width;
    int height;
    int delay;
    bool dither;

public:
    // The delay value is the time between frames in hundredths of a second (i.e. 2 means 50FPS)
    // NOTE: not all viewers pay much attention to this value.
    gif_writer(std::string const& filename, int width, int height, int delay, bool dither = true)
      : width(width), height(height), delay(delay), dither(dither)
    {
        gif_h::GifBegin(&writer, filename.c_str(), width, height, delay, 8, dither);
    }

    ~gif_writer() { gif_h::GifEnd(&writer); }

    void add(std::vector<uint8_t> const& data)
    {
        assert(data.size() == width * height * 4 && "data size does not match img size");
        add(data.data());
    }
    void add(uint8_t const* data) { gif_h::GifWriteFrame(&writer, data, width, height, delay, 8, dither); }
};
