#pragma once

/**
 * iterable and make_iterable from
 * https://github.com/CppCon/CppCon2014/blob/master/Presentations/C%2B%2B11%20in%20the%20Wild%20-%20Techniques%20from%20a%20Real%20Codebase/iterable.h
 *
 * CppCon2014/Presentations/C++11 in the Wild - Techniques from a Real Codebase/iterable.h
 *
 * Usage:
 *
 * int* values;
 * int valCount;
 *
 * for (_ val : make_iterable(values, values + valCount)) // all values
 *    ...;
 *
 * vector<int> vals;
 * for (_ val : make_iterable(vals.begin(), vals.end() - 1)) // all but last
 *    ...;
 *
 */

namespace _internal
{
template <class It>
class iterable
{
    It mFirst, mLast;

public:
    iterable() = default;
    iterable(It first, It last) : mFirst(first), mLast(last) {}
    It begin() const { return mFirst; }
    It end() const { return mLast; }
};
}

template <class It>
static inline _internal::iterable<It> make_iterable(It a, It b)
{
    return _internal::iterable<It>(a, b);
}
