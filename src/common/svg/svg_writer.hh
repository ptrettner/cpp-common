#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "elements.hh"
#include "filter.hh"
#include "gradient.hh"
#include "marker.hh"

namespace svg
{
struct svg_writer
{
    svg_writer(std::string const& filename, int width = -1, int height = -1);
    svg_writer(std::ostream& out, int width = -1, int height = -1);
    ~svg_writer();

    void set_width(int w) { width = w; }
    void set_height(int h) { height = h; }
    int get_width() const { return width; }
    int get_height() const { return height; }
    glm::ivec2 get_size() const { return {width, height}; }
    void set_size(int w, int h)
    {
        width = w;
        height = h;
    }
    void set_size(glm::ivec2 s)
    {
        width = s.x;
        height = s.y;
    }

    template <typename ElementT>
    svg_writer& operator<<(element<ElementT> const& e)
    {
        body << e;
        return *this;
    }

    svg_writer& operator<<(marker& m)
    {
        assert(!m.is_registered());

        ++markers;
        m.id = "marker" + std::to_string(markers);

        defs << "<marker";
        defs << " id=\"" << m.id << "\"";
        defs << " markerWidth=\"" << m.mSize.x << "\"";
        defs << " markerHeight=\"" << m.mSize.y << "\"";
        defs << " refX=\"" << m.mRef.x << "\"";
        defs << " refY=\"" << m.mRef.y << "\"";
        defs << " orient=\"" << (m.mOrient == marker::orient_auto ? "auto" : "auto-start-reverse") << "\"";
        defs << " markerUnits=\"" << (m.mUnits == marker::stroke_width ? "strokeWidth" : "userSpaceOnUse") << "\"";
        defs << ">\n";
        defs << m.body.str();
        defs << "</marker>\n";

        return *this;
    }

    svg_writer& operator<<(filter& f)
    {
        assert(!f.is_registered());

        ++filters;
        f.id = "filter" + std::to_string(filters);

        defs << "<filter";
        defs << " id=\"" << f.id << "\"";
        if (f.mAbsMode)
        {
            defs << " x=\"" << f.mArea.x << "\"";
            defs << " y=\"" << f.mArea.y << "\"";
            defs << " width=\"" << f.mArea.z << "\"";
            defs << " height=\"" << f.mArea.w << "\"";
        }
        else
        {
            defs << " x=\"" << f.mArea.x * 100 << "%\"";
            defs << " y=\"" << f.mArea.y * 100 << "%\"";
            defs << " width=\"" << f.mArea.z * 100 << "%\"";
            defs << " height=\"" << f.mArea.w * 100 << "%\"";
        }
        defs << ">\n";
        defs << f.body.str();
        defs << "</filter>\n";

        return *this;
    }

    svg_writer& operator<<(linear_gradient& g)
    {
        assert(!g.is_registered());

        ++gradients;
        g.id = "grad" + std::to_string(gradients);

        defs << "<linearGradient";
        defs << " id=\"" << g.id << "\"";
        defs << " x1=\"" << g.start.x * 100 << "%\"";
        defs << " y1=\"" << g.start.y * 100 << "%\"";
        defs << " x2=\"" << g.end.x * 100 << "%\"";
        defs << " y2=\"" << g.end.y * 100 << "%\"";
        defs << ">\n";
        defs << g.def.str();
        defs << "</linearGradient>\n";

        return *this;
    }

    void add_def(std::string const& d)
    {
        defs << d;
    }

private:
    std::ostream* tmp_out = nullptr;
    std::ostream* out = nullptr;

    std::stringstream defs;
    std::stringstream body;

    int width;
    int height;

    int markers = 0;
    int filters = 0;
    int gradients = 0;

    void write_header();
    void write_footer();
};
}
