#pragma once

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <cstddef>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <common/math/colors.hh>

#include <common/color/color.hh>

#include "filter.hh"
#include "gradient.hh"
#include "marker.hh"

/**
 * TODO:
 *  - http://tutorials.jenkov.com/svg/stroke.html
 *  - http://tutorials.jenkov.com/svg/marker-element.html
 *  - http://tutorials.jenkov.com/svg/path-element.html
 *  - tspan, tref, textpath, switch, image, a, defs, symbol, use
 *  - fillrule
 *  - animation, scripting
 *  - gradients
 *  - fill patterns
 *  - clip path
 *  - masks
 *  - filters
 */

namespace svg
{
template <typename ElementT>
struct element
{
    std::string name;
    std::string content;
    std::map<std::string, std::string> attributes;
    std::map<std::string, std::string> styles;
    std::vector<std::string> transforms;

    template <typename ValueT>
    ElementT& attr(std::string const& name, ValueT const& value)
    {
        std::stringstream ss;
        ss << value;
        attributes[name] = ss.str();
        return *static_cast<ElementT*>(this);
    }

    template <typename ValueT>
    ElementT& style(std::string const& name, ValueT const& value)
    {
        std::stringstream ss;
        ss << value;
        styles[name] = ss.str();
        return *static_cast<ElementT*>(this);
    }

    ElementT& stroke_dasharray(std::vector<float> const& dashes)
    {
        std::stringstream ss;
        for (auto d : dashes)
            ss << " " << d;
        style("stroke-dasharray", ss.str());
        return *static_cast<ElementT*>(this);
    }
    ElementT& stroke_width(float w)
    {
        style("stroke-width", w);
        return *static_cast<ElementT*>(this);
    }
    ElementT& stroke_opacity(float a)
    {
        style("stroke-opacity", a);
        return *static_cast<ElementT*>(this);
    }
    ElementT& stroke(std::nullptr_t)
    {
        style("stroke", "none");
        return *static_cast<ElementT*>(this);
    }
    ElementT& stroke(int rgb_hex)
    {
        style("stroke", glm::rgbToHex(rgb_hex));
        return *static_cast<ElementT*>(this);
    }
    ElementT& stroke(float r, float g, float b) { return stroke(glm::colorToRgb({r, g, b})); }
    ElementT& stroke(float r, float g, float b, float a) { return stroke(r, g, b).stroke_opacity(a); }
    ElementT& stroke(glm::vec3 c) { return stroke(c.r, c.g, c.b); }
    ElementT& stroke(glm::vec4 c) { return stroke(c.r, c.g, c.b, c.a); }
    ElementT& stroke(tinycolor::color c) { return stroke(c.to_srgba()); }

    ElementT& fill_opacity(float a)
    {
        style("fill-opacity", a);
        return *static_cast<ElementT*>(this);
    }
    ElementT& fill(std::nullptr_t)
    {
        style("fill", "none");
        return *static_cast<ElementT*>(this);
    }
    ElementT& fill(int rgb_hex)
    {
        style("fill", glm::rgbToHex(rgb_hex));
        return *static_cast<ElementT*>(this);
    }
    ElementT& fill(linear_gradient const& grad)
    {
        assert(grad.is_registered());
        style("fill", "url(#" + grad.id + ")");
        return *static_cast<ElementT*>(this);
    }
    ElementT& fill(float r, float g, float b) { return fill(glm::colorToRgb({r, g, b})); }
    ElementT& fill(float r, float g, float b, float a) { return fill(r, g, b).fill_opacity(a); }
    ElementT& fill(glm::vec3 c) { return fill(c.r, c.g, c.b); }
    ElementT& fill(glm::vec4 c) { return fill(c.r, c.g, c.b, c.a); }
    ElementT& fill(tinycolor::color c) { return fill(c.to_srgba()); }

    ElementT& filter(filter const& f)
    {
        assert(f.is_registered());
        attr("filter", "url(#" + f.id + ")");
        return *static_cast<ElementT*>(this);
    }

    ElementT& marker_start(marker const& m)
    {
        assert(m.is_registered());
        style("marker-start", "url(#" + m.id + ")");
        return *static_cast<ElementT*>(this);
    }
    ElementT& marker_mid(marker const& m)
    {
        assert(m.is_registered());
        style("marker-mid", "url(#" + m.id + ")");
        return *static_cast<ElementT*>(this);
    }
    ElementT& marker_end(marker const& m)
    {
        assert(m.is_registered());
        style("marker-end", "url(#" + m.id + ")");
        return *static_cast<ElementT*>(this);
    }
    ElementT& marker(marker const& start, marker const& end) { return marker_start(start).marker_end(end); }
    ElementT& marker(struct marker const& start, struct marker const& mid, struct marker const& end)
    {
        return marker_start(start).marker_mid(mid).marker_end(end);
    }

    ElementT& translate(float x, float y)
    {
        std::stringstream ss;
        ss << "translate(" << x << "," << y << ")";
        transforms.push_back(ss.str());
        return *static_cast<ElementT*>(this);
    }
    ElementT& translate(glm::vec2 p) { return translate(p.x, p.y); }

    ElementT& rotate(float cw_degree, float x = 0, float y = 0)
    {
        std::stringstream ss;
        ss << "rotate(" << cw_degree << "," << x << "," << y << ")";
        transforms.push_back(ss.str());
        return *static_cast<ElementT*>(this);
    }
    ElementT& rotate(float cw_degree, glm::vec2 p) { return rotate(cw_degree, p.x, p.y); }

    ElementT& scale(float sx, float sy)
    {
        std::stringstream ss;
        ss << "scale(" << sx << "," << sy << ")";
        transforms.push_back(ss.str());
        return *static_cast<ElementT*>(this);
    }
    ElementT& scale(float s) { return scale(s, s); }
    ElementT& scale(glm::vec2 s) { return scale(s.x, s.y); }

    ElementT& mirror_x() { return scale(-1, 1); }
    ElementT& mirror_y() { return scale(1, -1); }

    ElementT& transform_matrix(float a, float b, float c, float d, float e, float f)
    {
        std::stringstream ss;
        ss << "matrix(" << a << "," << b << "," << c << "," << d << "," << e << "," << f << ")";
        transforms.push_back(ss.str());
        return *static_cast<ElementT*>(this);
    }

    element(std::string const& name) : name(name) {}

    virtual void write_custom_attributes(std::ostream& out) const {}
};

struct rect : element<rect>
{
    rect(float x, float y, float w, float h) : element("rect")
    {
        attr("x", x);
        attr("y", y);
        attr("width", w);
        attr("height", h);
    }
    rect(glm::vec2 pos, glm::vec2 size) : rect(pos.x, pos.y, size.x, size.y) {}

    rect& rounded(float r) { return rounded(r, r); }
    rect& rounded(float rx, float ry)
    {
        attr("rx", rx);
        attr("ry", ry);
        return *this;
    }
};

struct circle : element<rect>
{
    circle(float cx, float cy, float r) : element("circle")
    {
        attr("cx", cx);
        attr("cy", cy);
        attr("r", r);
    }
    circle(glm::vec2 c, float r) : circle(c.x, c.y, r) {}
};

struct ellipse : element<rect>
{
    ellipse(float cx, float cy, float rx, float ry) : element("ellipse")
    {
        attr("cx", cx);
        attr("cy", cy);
        attr("rx", rx);
        attr("ry", ry);
    }
    ellipse(glm::vec2 c, glm::vec2 r) : ellipse(c.x, c.y, r.x, r.y) {}
};

struct line : element<rect>
{
    line(float x1, float y1, float x2, float y2) : element("line")
    {
        attr("x1", x1);
        attr("y1", y1);
        attr("x2", x2);
        attr("y2", y2);
    }
    line(glm::vec2 a, glm::vec2 b) : line(a.x, a.y, b.x, b.y) {}
};

struct polyline : element<rect>
{
    polyline(std::vector<glm::vec2> const& pts) : element("polyline")
    {
        std::stringstream ss;
        for (auto const& p : pts)
            ss << p.x << "," << p.y << "  ";
        attr("points", ss.str());
    }
};

struct polygon : element<rect>
{
    polygon(std::vector<glm::vec2> const& pts) : element("polygon")
    {
        std::stringstream ss;
        for (auto const& p : pts)
            ss << p.x << "," << p.y << "  ";
        attr("points", ss.str());
    }
};

/// <verb> -> relative
/// <verb>_to -> absolute
/// see https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
struct path : element<path>
{
    path() : element("path") {}
    path(float start_x, float start_y) : path() { move_to(start_x, start_y); }
    path(glm::vec2 start) : path() { move_to(start.x, start.y); }

    path& move(float dx, float dy)
    {
        d << "m " << dx << " " << dy << " ";
        return *this;
    }
    path& move_to(float x, float y)
    {
        d << "M " << x << " " << y << " ";
        return *this;
    }
    path& move(glm::vec2 dir) { return move(dir.x, dir.y); }
    path& move_to(glm::vec2 pos) { return move_to(pos.x, pos.y); }

    path& line(float dx, float dy)
    {
        d << "l " << dx << " " << dy << " ";
        return *this;
    }
    path& line_to(float x, float y)
    {
        d << "L " << x << " " << y << " ";
        return *this;
    }
    path& line(glm::vec2 dir) { return line(dir.x, dir.y); }
    path& line_to(glm::vec2 pos) { return line_to(pos.x, pos.y); }

    path& vline(float dy)
    {
        d << "v " << dy << " ";
        return *this;
    }
    path& vline_to(float y)
    {
        d << "V " << y << " ";
        return *this;
    }

    path& hline(float dx)
    {
        d << "h " << dx << " ";
        return *this;
    }
    path& hline_to(float x)
    {
        d << "H " << x << " ";
        return *this;
    }

    path& bezier(float dx1, float dy1, float dx2, float dy2, float dx, float dy)
    {
        d << "c " << dx1 << " " << dy1 << " " << dx2 << " " << dy2 << " " << dx << " " << dy << " ";
        return *this;
    }
    path& bezier_to(float x1, float y1, float x2, float y2, float x, float y)
    {
        d << "C " << x1 << " " << y1 << " " << x2 << " " << y2 << " " << x << " " << y << " ";
        return *this;
    }
    path& bezier(glm::vec2 dir1, glm::vec2 dir2, glm::vec2 dir)
    {
        return bezier(dir1.x, dir1.y, dir2.x, dir2.y, dir.x, dir.y);
    }
    path& bezier_to(glm::vec2 pos1, glm::vec2 pos2, glm::vec2 pos)
    {
        return bezier_to(pos1.x, pos1.y, pos2.x, pos2.y, pos.x, pos.y);
    }

    path& continue_bezier(float dx2, float dy2, float dx, float dy)
    {
        d << "s " << dx2 << " " << dy2 << " " << dx << " " << dy << " ";
        return *this;
    }
    path& continue_bezier_to(float x2, float y2, float x, float y)
    {
        d << "S " << x2 << " " << y2 << " " << x << " " << y << " ";
        return *this;
    }
    path& continue_bezier(glm::vec2 dir2, glm::vec2 dir) { return continue_bezier(dir2.x, dir2.y, dir.x, dir.y); }
    path& continue_bezier_to(glm::vec2 pos2, glm::vec2 pos) { return continue_bezier_to(pos2.x, pos2.y, pos.x, pos.y); }

    path& bezier(float dx1, float dy1, float dx, float dy)
    {
        d << "q " << dx1 << " " << dy1 << " " << dx << " " << dy << " ";
        return *this;
    }
    path& bezier_to(float x1, float y1, float x, float y)
    {
        d << "Q " << x1 << " " << y1 << " " << x << " " << y << " ";
        return *this;
    }
    path& bezier(glm::vec2 dir1, glm::vec2 dir) { return bezier(dir1.x, dir1.y, dir.x, dir.y); }
    path& bezier_to(glm::vec2 pos1, glm::vec2 pos) { return bezier_to(pos1.x, pos1.y, pos.x, pos.y); }

    path& continue_bezier(float dx, float dy)
    {
        d << "t " << dx << " " << dy << " ";
        return *this;
    }
    path& continue_bezier_to(float x, float y)
    {
        d << "T " << x << " " << y << " ";
        return *this;
    }
    path& continue_bezier(glm::vec2 dir) { return continue_bezier(dir.x, dir.y); }
    path& continue_bezier_to(glm::vec2 pos) { return continue_bezier_to(pos.x, pos.y); }

    path& arc(float rx, float ry, float x_axis_rot, bool large_arc, bool sweep, float dx, float dy)
    {
        d << "a " << rx << " " << ry << " " << x_axis_rot << " " << large_arc << " " << sweep << " " << dx << " " << dy << " ";
        return *this;
    }
    path& arc_to(float rx, float ry, float x_axis_rot, bool large_arc, bool sweep, float x, float y)
    {
        d << "A " << rx << " " << ry << " " << x_axis_rot << " " << large_arc << " " << sweep << " " << x << " " << y << " ";
        return *this;
    }
    path& arc(glm::vec2 radius, float x_axis_rot, bool large_arc, bool sweep, glm::vec2 dir)
    {
        return arc(radius.x, radius.y, x_axis_rot, large_arc, sweep, dir.x, dir.y);
    }
    path& arc_to(glm::vec2 radius, float x_axis_rot, bool large_arc, bool sweep, glm::vec2 pos)
    {
        return arc_to(radius.x, radius.y, x_axis_rot, large_arc, sweep, pos.x, pos.y);
    }

    path& close()
    {
        d << "Z ";
        return *this;
    }

    path& add(std::string const& cmd)
    {
        d << cmd << " ";
        return *this;
    }

    std::stringstream d;

    void write_custom_attributes(std::ostream& out) const override { out << " d=\"" << d.str() << "\" "; }
};

// TODO: path

struct text : element<text>
{
    enum anchor_dir
    {
        start,
        middle,
        end
    };

    text(float x, float y, std::string const& txt) : element("text")
    {
        attr("x", x);
        attr("y", y);
        content = txt;
    }
    text(glm::vec2 pos, std::string const& txt) : text(pos.x, pos.y, txt) {}

    text& anchor(anchor_dir d)
    {
        style("text-anchor", d == start ? "start" : d == middle ? "middle" : "end");
        return *this;
    }
    text& anchor_start() { return anchor(start); }
    text& anchor_middle() { return anchor(middle); }
    text& anchor_end() { return anchor(end); }

    text& kerning(float k)
    {
        style("kerning", k);
        return *this;
    }
    text& letter_spacing(float s)
    {
        style("letter-spacing", s);
        return *this;
    }
    text& word_spacing(float s)
    {
        style("word-spacing", s);
        return *this;
    }

    text& font_family(std::string const& f)
    {
        style("font-family", f);
        return *this;
    }
    text& font_size(float s)
    {
        style("font-size", s);
        return *this;
    }
    text& font(std::string const& f, float s) { return font_family(f).font_size(s); }
};

template <typename ElementT>
std::ostream& operator<<(std::ostream& out, element<ElementT> const& e)
{
    out << "<" << e.name;

    for (auto const& kvp : e.attributes)
        out << " " << kvp.first << "=\"" << kvp.second << "\" ";

    e.write_custom_attributes(out);

    out << " style=\"";
    for (auto const& kvp : e.styles)
        out << kvp.first << ":" << kvp.second << ";";
    out << "\"";

    if (!e.transforms.empty())
    {
        out << " transform=\"";
        for (auto const& t : e.transforms)
            out << t << " ";
        out << "\"";
    }

    if (e.content.empty())
        out << "/>\n";
    else
    {
        out << ">";
        out << e.content;
        out << "</" << e.name << ">";
    }

    return out;
}

template <typename ElementT>
marker& marker::operator<<(element<ElementT> const& e)
{
    body << e;
    return *this;
}
}
