#include "svg_writer.hh"

using namespace svg;

svg_writer::svg_writer(const std::string &filename, int width, int height) : width(width), height(height)
{
    tmp_out = new std::ofstream(filename);
    out = tmp_out;
}

svg_writer::svg_writer(std::ostream &out, int width, int height) : width(width), height(height) { this->out = &out; }

svg_writer::~svg_writer()
{
    write_header();
    *out << "<defs>\n";
    *out << defs.str();
    *out << "</defs>\n";
    *out << body.str();
    write_footer();
    delete tmp_out;
}

void svg_writer::write_header()
{
    // *out << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
    *out << "<svg version=\"1.1\" baseProfile=\"full\" ";
    if (width > 0)
        *out << "width=\"" << width << "\" ";
    if (height > 0)
        *out << "height=\"" << height << "\" ";
    *out << " xmlns=\"http://www.w3.org/2000/svg\">\n";
}

void svg_writer::write_footer() { *out << "</svg>\n"; }
