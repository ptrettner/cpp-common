#pragma once

#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <glm/glm.hpp>

#include <common/color/color.hh>

namespace svg
{
template <class ElementT>
struct element;

/**
 * Usage:
 *
 * auto grad = linear_gradient()
 *  .stop(0.0, color(...))
 *  .stop(0.5, color(...));
 *  .stop(1.0, color(...));
 *
 * svg << grad;
 * svg << polygon(...).fill(grad);
 *
 * TODO: abs mode
 */
struct linear_gradient
{
    // linear_gradient() = default;

    // e.g. {0,0}, {1,0}
    linear_gradient(glm::vec2 start, glm::vec2 end) : start(start), end(end) {}

    linear_gradient& stop(float s, float r, float g, float b, float a = 1.0f)
    {
        def << "<stop offset=\"" << s * 100 << "%\" style=\"stop-color:rgb(";
        def << int(r * 255) << ",";
        def << int(g * 255) << ",";
        def << int(b * 255);
        def << ");stop-opacity:" << a << "\" />\n";
        return *this;
    }
    linear_gradient& stop(float s, glm::vec3 c, float a = 1.0f) { return stop(s, c.x, c.y, c.z, a); }
    linear_gradient& stop(float s, tinycolor::color c, float a) { return stop(s, c.to_srgb(), a); }
    linear_gradient& stop(float s, tinycolor::color c) { return stop(s, c.to_srgba()); }
    linear_gradient& stop(float s, glm::vec4 c) { return stop(s, c.x, c.y, c.z, c.a); }

    bool is_registered() const { return !id.empty(); }

private:
    std::string id;
    glm::vec2 start;
    glm::vec2 end;
    std::stringstream def;

    friend struct svg_writer;

    template <class ElementT>
    friend struct element;
};
}
