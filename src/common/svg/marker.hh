#pragma once

#include <glm/glm.hpp>

#include <cassert>
#include <sstream>
#include <string>

namespace svg
{
template <typename ElementT>
struct element;

/**
 * SVG Markers
 *
 * Usage:
 *
 *    auto m = marker(...)
 *    m << circle(0,0,5); // builder marker
 *    svg << m; // registers the marker
 *    svg << line(...).marker_start(m); // use the marker
 */
struct marker
{
public:
    enum orient_t
    {
        orient_auto,
        orient_auto_start_reverse
    };
    enum units_t
    {
        stroke_width,
        user_space
    };

public:
    marker(float width, float height, float ref_x = 0, float ref_y = 0, orient_t orient = orient_auto, units_t units = stroke_width)
      : marker({width, height}, {ref_x, ref_y}, orient, units)
    {
    }
    marker(glm::vec2 size, glm::vec2 ref = {0, 0}, orient_t orient = orient_auto, units_t units = stroke_width)
      : mSize(size), mRef(ref), mOrient(orient), mUnits(units)
    {
    }

    marker& width(float w)
    {
        assert(!is_registered());
        mSize.x = w;
        return *this;
    }
    marker& height(float h)
    {
        assert(!is_registered());
        mSize.y = h;
        return *this;
    }
    marker& size(float w, float h)
    {
        assert(!is_registered());
        mSize = {w, h};
        return *this;
    }
    marker& size(glm::vec2 s)
    {
        assert(!is_registered());
        mSize = s;
        return *this;
    }
    marker& ref(float x, float y)
    {
        assert(!is_registered());
        mRef = {x, y};
        return *this;
    }
    marker& ref(glm::vec2 ref)
    {
        assert(!is_registered());
        mRef = ref;
        return *this;
    }
    marker& orient(orient_t orient)
    {
        assert(!is_registered());
        mOrient = orient;
        return *this;
    }
    marker& units(units_t units)
    {
        assert(!is_registered());
        mUnits = units;
        return *this;
    }

    template <typename ElementT>
    marker& operator<<(element<ElementT> const& e);

    bool is_registered() const { return !id.empty(); }

private:
    glm::vec2 mSize;
    glm::vec2 mRef;
    orient_t mOrient;
    units_t mUnits;

    std::string id;
    std::stringstream body;

    friend struct svg_writer;

    template <typename ElementT>
    friend struct element;
};
}
