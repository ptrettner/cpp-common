#pragma once

#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <glm/glm.hpp>

namespace svg
{
template <class ElementT>
struct filter_element;

/**
 * SVG Filters
 *
 * Filters:
 *    - fe_offset
 *    - fe_blend
 *    - fe_gaussian_blur
 *    - fe_merge
 *    - fe_composite
 *
 * Usage:
 *
 *    auto f = filter(...)
 *    m << fe_offset(...); // build filter
 *    svg << f; // registers the filter
 *    svg << line(...).filter(f); // use the filter
 */
struct filter
{
public:
    static constexpr char const* src_graphic = "SourceGraphic";
    static constexpr char const* src_alpha = "SourceAlpha";
    static constexpr char const* bg_image = "BackgroundImage";
    static constexpr char const* bg_alpha = "BackgroundAlpha";
    static constexpr char const* fill_paint = "FillPaint";
    static constexpr char const* stroke_paint = "StrokePaint";

public:
    filter(glm::vec4 area = {-.1f, -.1f, 1.2f, 1.2f}, bool abs_mode = false) : mArea(area), mAbsMode(abs_mode) {}
    filter(float x, float y, float w, float h, bool abs_mode = false) : filter({x, y, w, h}, abs_mode) {}

    filter& abs_mode()
    {
        mAbsMode = true;
        return *this;
    }
    filter& rel_mode()
    {
        mAbsMode = false;
        return *this;
    }
    filter& area(glm::vec4 a)
    {
        mArea = a;
        return *this;
    }
    filter& area(float x, float y, float w, float h)
    {
        mArea = {x, y, w, h};
        return *this;
    }

    template <typename ElementT>
    filter& operator<<(filter_element<ElementT> const& e)
    {
        body << e;
        return *this;
    }

    bool is_registered() const { return !id.empty(); }

private:
    glm::vec4 mArea;
    bool mAbsMode;

    std::string id;
    std::stringstream body;

    friend struct svg_writer;

    template <typename ElementT>
    friend struct element;
};

template <class ElementT>
struct filter_element
{
    std::string name;
    std::stringstream content;
    std::map<std::string, std::string> attributes;

    template <typename ValueT>
    ElementT& attr(std::string const& name, ValueT const& value)
    {
        std::stringstream ss;
        ss << value;
        attributes[name] = ss.str();
        return *static_cast<ElementT*>(this);
    }

    ElementT& result(std::string const& name) { return attr("result", name); }
    ElementT& in(std::string const& name) { return attr("in", name); }

    filter_element(std::string const& name) : name(name) {}
};

struct fe_offset : filter_element<fe_offset>
{
    fe_offset(glm::vec2 offset) : filter_element("feOffset")
    {
        attr("dx", offset.x);
        attr("dy", offset.y);
    }

    fe_offset(float dx, float dy) : fe_offset(glm::vec2(dx, dy)) {}
};

struct fe_gaussian_blur : filter_element<fe_gaussian_blur>
{
    fe_gaussian_blur(float std_dev) : filter_element("feGaussianBlur") { attr("stdDeviation", std_dev); }
};

struct fe_merge : filter_element<fe_merge>
{
    fe_merge(std::vector<std::string> const& inputs) : filter_element("feMerge")
    {
        for (auto const& n : inputs)
            content << "<feMergeNode in=\"" << n << "\"/>\n";
    }

    fe_merge& add(std::string const& input)
    {
        content << "<feMergeNode in=\"" << input << "\"/>\n";
        return *this;
    }
};

// result = k1*i1*i2 + k2*i1 + k3*i2 + k4
struct fe_composite : filter_element<fe_composite>
{
    enum op_t
    {
        op_over,
        op_in,
        op_out,
        op_atop,
        op_xor,
        op_arithmetic
    };

    fe_composite(op_t o) : filter_element("feComposite") { op(o); }

    fe_composite(float k1, float k2, float k3, float k4) : fe_composite(op_arithmetic)
    {
        attr("k1", k1);
        attr("k2", k2);
        attr("k3", k3);
        attr("k4", k4);
    }

    fe_composite& op(op_t o)
    {
        switch (o)
        {
        case op_over:
            attr("operation", "over");
            break;
        case op_in:
            attr("operation", "in");
            break;
        case op_out:
            attr("operation", "out");
            break;
        case op_atop:
            attr("operation", "atop");
            break;
        case op_xor:
            attr("operation", "xor");
            break;
        case op_arithmetic:
            attr("operation", "arithmetic");
            break;
        }

        return *this;
    }

    fe_composite& in2(std::string const& name) { return attr("in2", name); }
    using filter_element<fe_composite>::in;
    fe_composite& in(std::string const& in1, std::string const& in2) { return attr("in", in1).attr("in2", in2); }
};

struct fe_blend : filter_element<fe_blend>
{
    enum blend_mode
    {
        normal,
        multiply,
        screen,
        darken,
        lighten
    };

    fe_blend& in2(std::string const& name) { return attr("in2", name); }
    using filter_element<fe_blend>::in;
    fe_blend& in(std::string const& in1, std::string const& in2) { return attr("in", in1).attr("in2", in2); }

    fe_blend(blend_mode mode = normal) : filter_element("feBlend")
    {
        switch (mode)
        {
        case normal:
            attr("blend", "normal");
            break;
        case multiply:
            attr("blend", "multiply");
            break;
        case screen:
            attr("blend", "screen");
            break;
        case darken:
            attr("blend", "darken");
            break;
        case lighten:
            attr("blend", "lighten");
            break;
        }
    }

    fe_blend(std::string in1, std::string in2, blend_mode mode = normal) : fe_blend(mode) { in(in1, in2); }
};

template <typename ElementT>
std::ostream& operator<<(std::ostream& out, filter_element<ElementT> const& e)
{
    out << "<" << e.name;

    for (auto const& kvp : e.attributes)
        out << " " << kvp.first << "=\"" << kvp.second << "\"";

    auto c = e.content.str();
    if (c.empty())
        out << "/>\n";
    else
    {
        out << ">";
        out << c;
        out << "</" << e.name << ">";
    }

    return out;
}
}
