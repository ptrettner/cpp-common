#pragma once

#include <glm/vec3.hpp>

#include "direction.hh"

namespace grid
{
struct edge_coords
{
    glm::ivec3 start;
    direction dir;
};

struct face_coords
{
    glm::ivec3 start;
    direction normal;
};

inline int index_count(glm::ivec3 cnt)
{
    return cnt.x * cnt.y * cnt.z;
}
inline glm::ivec3 decode_ivec3(int idx, glm::ivec3 cnt)
{
    auto z = idx % cnt.z;
    idx /= cnt.z;
    auto y = idx % cnt.y;
    idx /= cnt.y;
    auto x = idx;

    return {x, y, z};
}
inline int encode_ivec3(glm::ivec3 coords, glm::ivec3 cnt)
{
    return (coords.x * cnt.y + coords.y) * cnt.z + coords.z;
}

inline glm::ivec3 decode_cell(int idx, glm::ivec3 cellCount)
{
    return decode_ivec3(idx, cellCount);
}
inline int encode_cell(glm::ivec3 coords, glm::ivec3 cellCount)
{
    return encode_ivec3(coords, cellCount);
}

inline glm::ivec3 decode_node(int idx, glm::ivec3 nodeCount)
{
    return decode_ivec3(idx, nodeCount);
}
inline int encode_node(glm::ivec3 coords, glm::ivec3 nodeCount)
{
    return encode_ivec3(coords, nodeCount);
}

inline edge_coords decode_edge(int idx, glm::ivec3 nodeCount)
{
    auto xDirCounts = nodeCount - glm::ivec3(1, 0, 0);
    auto yDirCounts = nodeCount - glm::ivec3(0, 1, 0);
    auto zDirCounts = nodeCount - glm::ivec3(0, 0, 1);

    auto cntY = index_count(yDirCounts);
    auto cntZ = index_count(zDirCounts);

    if (idx < cntZ)
        return {decode_ivec3(idx, zDirCounts), DirZ};
    else if (idx < cntZ + cntY)
        return {decode_ivec3(idx - cntZ, yDirCounts), DirY};
    else
        return {decode_ivec3(idx - cntZ - cntY, xDirCounts), DirX};
}
inline int encode_edge(edge_coords coords, glm::ivec3 nodeCount)
{
    auto xDirCounts = nodeCount - glm::ivec3(1, 0, 0);
    auto yDirCounts = nodeCount - glm::ivec3(0, 1, 0);
    auto zDirCounts = nodeCount - glm::ivec3(0, 0, 1);

    auto cntY = index_count(yDirCounts);
    auto cntZ = index_count(zDirCounts);

    switch (coords.dir)
    {
    case DirX:
        return encode_ivec3(coords.start, xDirCounts) + cntZ + cntY;
    case DirY:
        return encode_ivec3(coords.start, yDirCounts) + cntZ;
    case DirZ:
        return encode_ivec3(coords.start, zDirCounts);
    default:
        assert(0 && "invalid dir");
        return -1;
    }
}

inline face_coords decode_face(int idx, glm::ivec3 cellCount)
{
    auto xDirCounts = cellCount + glm::ivec3(1, 0, 0);
    auto yDirCounts = cellCount + glm::ivec3(0, 1, 0);
    auto zDirCounts = cellCount + glm::ivec3(0, 0, 1);

    auto cntY = index_count(yDirCounts);
    auto cntZ = index_count(zDirCounts);

    if (idx < cntZ)
        return {decode_ivec3(idx, zDirCounts), DirZ};
    else if (idx < cntZ + cntY)
        return {decode_ivec3(idx - cntZ, yDirCounts), DirY};
    else
        return {decode_ivec3(idx - cntZ - cntY, xDirCounts), DirX};
}
inline int encode_face(face_coords coords, glm::ivec3 cellCount)
{
    auto xDirCounts = cellCount + glm::ivec3(1, 0, 0);
    auto yDirCounts = cellCount + glm::ivec3(0, 1, 0);
    auto zDirCounts = cellCount + glm::ivec3(0, 0, 1);

    auto cntY = index_count(yDirCounts);
    auto cntZ = index_count(zDirCounts);

    switch (coords.normal)
    {
    case DirX:
        return encode_ivec3(coords.start, xDirCounts) + cntZ + cntY;
    case DirY:
        return encode_ivec3(coords.start, yDirCounts) + cntZ;
    case DirZ:
        return encode_ivec3(coords.start, zDirCounts);
    default:
        assert(0 && "invalid dir");
        return -1;
    }
}
}
