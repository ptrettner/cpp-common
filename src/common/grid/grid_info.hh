#pragma once

#include <glm/vec3.hpp>

namespace grid
{
struct grid_info
{
    int cellsX = 0;
    int cellsY = 0;
    int cellsZ = 0;
    glm::ivec3 cellCount() const { return {cellsX, cellsY, cellsZ}; }

    grid_info() = default;
    grid_info(glm::ivec3 c) : cellsX(c.x), cellsY(c.y), cellsZ(c.z) {}

    bool is_valid() const { return cellsX > 0 && cellsY > 0 && cellsZ > 0; }

    bool operator==(grid_info const& rhs) const { return cellCount() == rhs.cellCount(); }
    bool operator!=(grid_info const& rhs) const { return !operator==(rhs); }
};
}
