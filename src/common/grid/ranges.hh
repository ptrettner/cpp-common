#pragma once

#include <glm/vec3.hpp>

#include "cursors.hh"
#include "iterators.hh"
#include "math.hh"

namespace grid
{
struct cell_range
{
    explicit cell_range(glm::ivec3 cellCount) : cellCount(cellCount) {}

    // properties
public:
    glm::ivec3 shape() const { return cellCount; }
    int size() const { return cellCount.x * cellCount.y * cellCount.z; }

    // methods
public:
    cell_index idxOf(int x, int y, int z) const { return idxOf({x, y, z}); }
    cell_index idxOf(glm::ivec3 p) const { return cell_index(encode_cell(p, cellCount)); }

    // range
public:
    all_cell_iterator begin() const { return cell_handle({cellCount}, cell_index(0)); }
    all_cell_iterator end() const { return cell_handle({cellCount}, cell_index(size())); }

private:
    glm::ivec3 cellCount;
};

struct node_range
{
    explicit node_range(glm::ivec3 nodeCount) : nodeCount(nodeCount) {}

    // properties
public:
    glm::ivec3 shape() const { return nodeCount; }
    int size() const { return nodeCount.x * nodeCount.y * nodeCount.z; }

    // methods
public:
    node_index idxOf(int x, int y, int z) const { return idxOf({x, y, z}); }
    node_index idxOf(glm::ivec3 p) const { return node_index(encode_node(p, nodeCount)); }

    // range
public:
    all_node_iterator begin() const { return node_handle({nodeCount - 1}, node_index(0)); }
    all_node_iterator end() const { return node_handle({nodeCount - 1}, node_index(size())); }

private:
    glm::ivec3 nodeCount;
};

struct edge_range
{
    explicit edge_range(glm::ivec3 nodeCount) : nodeCount(nodeCount) {}

    // properties
public:
    int size() const
    {
        return nodeCount.x * nodeCount.y * (nodeCount.z - 1) + //
               nodeCount.x * (nodeCount.y - 1) * nodeCount.z + //
               (nodeCount.x - 1) * nodeCount.y * nodeCount.z;
    }

    // methods
public:
    edge_index idxOf(int x, int y, int z, direction dir) const { return idxOf({x, y, z}, dir); }
    edge_index idxOf(glm::ivec3 p, direction dir) const { return edge_index(encode_edge({p, dir}, nodeCount)); }

    // range
public:
    all_edge_iterator begin() const { return edge_handle({nodeCount - 1}, edge_index(0)); }
    all_edge_iterator end() const { return edge_handle({nodeCount - 1}, edge_index(size())); }

private:
    glm::ivec3 nodeCount;
};

struct face_range
{
    explicit face_range(glm::ivec3 cellCount) : cellCount(cellCount) {}

    // properties
public:
    int size() const
    {
        return cellCount.x * cellCount.y * (cellCount.z + 1) + //
               cellCount.x * (cellCount.y + 1) * cellCount.z + //
               (cellCount.x + 1) * cellCount.y * cellCount.z;
    }

    // methods
public:
    face_index idxOf(int x, int y, int z, direction dir) const { return idxOf({x, y, z}, dir); }
    face_index idxOf(glm::ivec3 p, direction dir) const { return face_index(encode_face({p, dir}, cellCount)); }

    // range
public:
    all_face_iterator begin() const { return face_handle({cellCount}, face_index(0)); }
    all_face_iterator end() const { return face_handle({cellCount}, face_index(size())); }

private:
    glm::ivec3 cellCount;
};
}
