#pragma once

namespace grid
{
struct grid;

struct cell_tag;
struct node_tag;
struct edge_tag;
struct face_tag;

template <class AttrT>
struct cell_attribute;
template <class AttrT>
struct node_attribute;
template <class AttrT>
struct face_attribute;
template <class AttrT>
struct edge_attribute;

template <typename tag>
struct primitive_iterator;

struct cell_index;
struct node_index;
struct face_index;
struct edge_index;

struct cell_handle;
struct node_handle;
struct face_handle;
struct edge_handle;

struct cell_range;
struct node_range;
struct face_range;
struct edge_range;
}
