#pragma once

#include <memory>

#include <glm/vec3.hpp>

#include "cursors.hh"
#include "fwd.hh"
#include "iterators.hh"
#include "primitives.hh"

namespace grid
{
template <class tag, class AttrT>
struct primitive_attribute
{
    template <class A>
    using attribute = typename primitive<tag>::template attribute<A>;
    using index_t = typename primitive<tag>::index;
    using handle_t = typename primitive<tag>::handle;
    using tag_t = tag;

    // members
private:
    grid_info mInfo;
    std::unique_ptr<AttrT[]> mData;

    // properties
public:
    bool is_valid() const { return mInfo.is_valid(); }

    AttrT* data() { return mData.get(); }
    AttrT const* data() const { return mData.get(); }

    int size() const { return primitive<tag>::size_of(grid(mInfo)); }

    AttrT& operator[](index_t idx)
    {
        assert(0 <= idx.value && idx.value < size());
        return this->data()[idx.value];
    }
    AttrT const& operator[](index_t idx) const
    {
        assert(0 <= idx.value && idx.value < size());
        return this->data()[idx.value];
    }
    AttrT& operator[](handle_t h) { return operator[](h.idx); }
    AttrT const& operator[](handle_t h) const { return operator[](h.idx); }

    // methods
public:
    void fill(AttrT const& v);

    // range
public:
    attribute_iterator<primitive_attribute&> begin() { return {0, *this}; }
    attribute_iterator<primitive_attribute const&> begin() const { return {0, *this}; }
    attribute_iterator<primitive_attribute&> end() { return {size(), *this}; }
    attribute_iterator<primitive_attribute const&> end() const { return {size(), *this}; }

    // ctor
public:
    primitive_attribute() = default;
    primitive_attribute(grid const& grid, AttrT const& def = {});
};

template <class AttrT>
struct cell_attribute : primitive_attribute<cell_tag, AttrT>
{
    using primitive_attribute<cell_tag, AttrT>::primitive_attribute;
};
template <class AttrT>
struct node_attribute : primitive_attribute<node_tag, AttrT>
{
    using primitive_attribute<node_tag, AttrT>::primitive_attribute;
};
template <class AttrT>
struct edge_attribute : primitive_attribute<edge_tag, AttrT>
{
    using primitive_attribute<edge_tag, AttrT>::primitive_attribute;
};
template <class AttrT>
struct face_attribute : primitive_attribute<face_tag, AttrT>
{
    using primitive_attribute<face_tag, AttrT>::primitive_attribute;
};
}
