#pragma once

#include "cursors.hh"

namespace grid
{
template <typename tag>
struct all_primitive_iterator
{
    using handle_t = typename primitive<tag>::handle;

    all_primitive_iterator() = default;
    all_primitive_iterator(handle_t handle) : handle(handle) {}

    handle_t operator*() const { return handle; }
    all_primitive_iterator& operator++()
    {
        ++handle.idx.value;
        return *this;
    }
    all_primitive_iterator operator++(int)
    {
        auto i = *this;
        operator++();
        return i;
    }
    bool operator==(all_primitive_iterator const& rhs) const
    {
        assert(handle.info == rhs.handle.info && "comparing iterators from different grids");
        return handle.idx == rhs.handle.idx;
    }
    bool operator!=(all_primitive_iterator const& rhs) const { return !operator==(rhs); }

private:
    handle_t handle;
};
using all_cell_iterator = all_primitive_iterator<cell_tag>;
using all_node_iterator = all_primitive_iterator<node_tag>;
using all_edge_iterator = all_primitive_iterator<edge_tag>;
using all_face_iterator = all_primitive_iterator<face_tag>;

// ================= ATTRIBUTES =================

template <class AttributeT>
struct attribute_iterator final
{
    int idx;
    AttributeT attr;

    auto operator*() const -> decltype(attr.data()[idx]) { return attr.data()[idx]; }

    attribute_iterator& operator++()
    {
        ++idx;
        return *this;
    }
    attribute_iterator operator++(int)
    {
        auto i = *this;
        operator++();
        return i;
    }
    bool operator==(attribute_iterator const& rhs) const { return idx == rhs.idx; }
    bool operator!=(attribute_iterator const& rhs) const { return idx != rhs.idx; }
};
}
