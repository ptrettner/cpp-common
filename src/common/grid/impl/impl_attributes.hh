#pragma once

#include "../attributes.hh"
#include "../grid.hh"

namespace grid
{
template <class tag, class AttrT>
primitive_attribute<tag, AttrT>::primitive_attribute(const grid &grid, AttrT const &def)
  : //
    mInfo(grid.info()),
    mData(new AttrT[size()])
{
    fill(def);
}

template <class tag, class AttrT>
void primitive_attribute<tag, AttrT>::fill(AttrT const &v)
{
    auto s = this->size();
    auto d = this->data();
    for (auto i = 0; i < s; ++i)
        d[i] = v;
}
}
