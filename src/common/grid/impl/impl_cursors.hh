#pragma once

#include "../cursors.hh"
#include "../grid.hh"
#include "../math.hh"

namespace grid
{
template <class tag>
primitive_handle<tag>::primitive_handle(struct grid const& grid, index_t idx) : info(grid.info()), idx(idx)
{
}

template <class tag>
grid primitive_handle<tag>::grid() const
{
    return {info};
}

inline glm::ivec3 cell_handle::coords() const
{
    return decode_cell(idx.value, info.cellCount());
}
inline glm::ivec3 node_handle::coords() const
{
    return decode_node(idx.value, info.cellCount() + 1);
}
inline edge_coords edge_handle::coords() const
{
    return decode_edge(idx.value, info.cellCount() + 1);
}
inline face_coords face_handle::coords() const
{
    return decode_face(idx.value, info.cellCount());
}

inline bool cell_handle::is_boundary() const
{
    auto c = coords();
    auto m = info.cellCount() - 1;
    return c.x == 0 || c.y == 0 || c.z == 0 || //
           c.x == m.x || c.y == m.y || c.z == m.z;
}
inline bool node_handle::is_boundary() const
{
    auto c = coords();
    auto m = info.cellCount();
    return c.x == 0 || c.y == 0 || c.z == 0 || //
           c.x == m.x || c.y == m.y || c.z == m.z;
}
inline bool edge_handle::is_boundary() const
{
    auto c = coords().start;
    auto m = info.cellCount() - dir3();
    return c.x == 0 || c.y == 0 || c.z == 0 || //
           c.x == m.x || c.y == m.y || c.z == m.z;
}
inline bool face_handle::is_boundary() const
{
    auto c = coords().start;
    auto m = info.cellCount() - 1 + normal3();
    return c.x == 0 || c.y == 0 || c.z == 0 || //
           c.x == m.x || c.y == m.y || c.z == m.z;
}

inline std::array<node_handle, 8> cell_handle::nodes() const
{
    auto c = coords();
    auto g = grid();
    return {
        g.node(c + glm::ivec3(0, 0, 0)), //
        g.node(c + glm::ivec3(0, 0, 1)), //
        g.node(c + glm::ivec3(0, 1, 0)), //
        g.node(c + glm::ivec3(0, 1, 1)), //
        g.node(c + glm::ivec3(1, 0, 0)), //
        g.node(c + glm::ivec3(1, 0, 1)), //
        g.node(c + glm::ivec3(1, 1, 0)), //
        g.node(c + glm::ivec3(1, 1, 1)), //
    };
}
inline std::array<face_handle, 6> cell_handle::faces() const
{
    auto c = coords();
    auto g = grid();
    return {
        g.face(c + glm::ivec3(0, 0, 0), DirX), //
        g.face(c + glm::ivec3(0, 0, 0), DirY), //
        g.face(c + glm::ivec3(0, 0, 0), DirZ), //

        g.face(c + glm::ivec3(1, 0, 0), DirX), //
        g.face(c + glm::ivec3(0, 1, 0), DirY), //
        g.face(c + glm::ivec3(0, 0, 1), DirZ), //
    };
}
inline std::array<edge_handle, 12> cell_handle::edges() const
{
    auto c = coords();
    auto g = grid();
    return {
        g.edge(c + glm::ivec3(0, 0, 0), DirX), //
        g.edge(c + glm::ivec3(0, 0, 1), DirX), //
        g.edge(c + glm::ivec3(0, 1, 0), DirX), //
        g.edge(c + glm::ivec3(0, 1, 1), DirX), //

        g.edge(c + glm::ivec3(0, 0, 0), DirY), //
        g.edge(c + glm::ivec3(0, 0, 1), DirY), //
        g.edge(c + glm::ivec3(1, 0, 0), DirY), //
        g.edge(c + glm::ivec3(1, 0, 1), DirY), //

        g.edge(c + glm::ivec3(0, 0, 0), DirZ), //
        g.edge(c + glm::ivec3(0, 1, 0), DirZ), //
        g.edge(c + glm::ivec3(1, 0, 0), DirZ), //
        g.edge(c + glm::ivec3(1, 1, 0), DirZ), //
    };
}

inline node_handle face_handle::node_start() const
{
    return grid().node(coords().start);
}

inline edge_handle node_handle::edge_pos(direction dir) const
{
    return grid().edge(coords(), dir);
}
inline edge_handle node_handle::edge_neg(direction dir) const
{
    return grid().edge(coords() - dir3(dir), dir);
}

inline std::array<cell_handle, 2> face_handle::cells() const
{
    // TODO: check if valid
    auto c = coords();
    auto g = grid();
    return {
        g.cell(c.start - dir3(c.normal)), //
        g.cell(c.start),                  //
    };
}

inline std::array<node_handle, 4> face_handle::nodes() const
{
    auto c = coords();
    auto g = grid();
    auto d0 = tangent3(c.normal);
    auto d1 = bitangent3(c.normal);
    return {
        g.node(c.start),           //
        g.node(c.start + d0),      //
        g.node(c.start + d0 + d1), //
        g.node(c.start + d1),      //
    };
}

inline std::array<edge_handle, 4> face_handle::edges() const
{
    auto c = coords();
    auto g = grid();
    auto d0 = tangent3(c.normal);
    auto d1 = bitangent3(c.normal);
    auto dir0 = tangent(c.normal);
    auto dir1 = bitangent(c.normal);
    assert(d0 == ::grid::dir3(dir0));
    assert(d1 == ::grid::dir3(dir1));
    return {
        g.edge(c.start, dir0),      //
        g.edge(c.start + d0, dir1), //
        g.edge(c.start + d1, dir0), //
        g.edge(c.start, dir1),      //
    };
}

inline std::array<node_handle, 2> edge_handle::nodes() const
{
    auto c = coords();
    auto g = grid();
    return {
        g.node(c.start),                       //
        g.node(c.start + ::grid::dir3(c.dir)), //
    };
}

inline std::array<cell_handle, 4> edge_handle::cells() const
{
    auto c = coords();
    auto g = grid();
    auto d0 = tangent3(c.dir);
    auto d1 = bitangent3(c.dir);
    return {
        g.cell(c.start - d0 - d1), //
        g.cell(c.start - d0),      //
        g.cell(c.start),           //
        g.cell(c.start - d1),      //
    };
}

inline node_handle cell_handle::node(int dx, int dy, int dz) const
{
    return grid().node(coords() + glm::ivec3(dx, dy, dz));
}

inline cell_handle face_handle::cellA() const
{
    auto c = coords();
    auto ci = c.start - dir3(c.normal);
    if (ci.x < 0 || ci.y < 0 || ci.z < 0)
        return cell_handle(info, cell_index::invalid());
    return grid().cell(ci);
}
inline cell_handle face_handle::cellB() const
{
    auto c = coords();
    auto ci = c.start;
    if (ci.x >= info.cellsX || ci.y >= info.cellsY || ci.z >= info.cellsZ)
        return cell_handle(info, cell_index::invalid());
    return grid().cell(ci);
}

inline node_handle edge_handle::nodeA() const
{
    auto c = coords();
    return grid().node(c.start);
}
inline node_handle edge_handle::nodeB() const
{
    auto c = coords();
    return grid().node(c.start + ::grid::dir3(c.dir));
}
}
