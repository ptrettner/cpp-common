#pragma once

#include "../grid.hh"
#include "../primitives.hh"

namespace grid
{
inline int primitive<cell_tag>::size_of(const grid &grid) { return grid.cells().size(); }
inline int primitive<node_tag>::size_of(const grid &grid) { return grid.nodes().size(); }
inline int primitive<edge_tag>::size_of(const grid &grid) { return grid.edges().size(); }
inline int primitive<face_tag>::size_of(const grid &grid) { return grid.faces().size(); }
}
