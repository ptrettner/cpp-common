#pragma once

#include <glm/vec3.hpp>

namespace grid
{
enum direction
{
    DirX = 0,
    DirY = 1,
    DirZ = 2
};

inline direction tangent(direction d)
{
    return direction((d + 2) % 3);
}

inline direction bitangent(direction d)
{
    return direction((d + 1) % 3);
}

inline glm::ivec3 dir3(direction d)
{
    return glm::ivec3(d == DirX, d == DirY, d == DirZ);
}

inline glm::ivec3 tangent3(direction d)
{
    return glm::ivec3(d == DirY, d == DirZ, d == DirX);
}

inline glm::ivec3 bitangent3(direction d)
{
    return glm::ivec3(d == DirZ, d == DirX, d == DirY);
}
}
