#pragma once

#include <array>
#include <iostream>

#include <glm/vec3.hpp>

#include "fwd.hh"
#include "grid_info.hh"
#include "math.hh"
#include "primitives.hh"
#include "tmp.hh"

namespace grid
{
// ======================== BASE ========================

template <class tag>
struct primitive_index
{
    template <class AttrT>
    using attribute = typename primitive<tag>::template attribute<AttrT>;
    using index_t = typename primitive<tag>::index;
    using handle_t = typename primitive<tag>::handle;

    int value = -1;

    primitive_index() = default;
    explicit primitive_index(int idx) : value(idx) {}

    bool is_valid() const { return value >= 0; }
    bool is_invalid() const { return value < 0; }
    static index_t invalid() { return {}; }

    bool operator<(index_t const& rhs) const { return value < rhs.value; }
    bool operator<=(index_t const& rhs) const { return value <= rhs.value; }
    bool operator>(index_t const& rhs) const { return value > rhs.value; }
    bool operator>=(index_t const& rhs) const { return value >= rhs.value; }
    bool operator==(index_t const& rhs) const { return value == rhs.value; }
    bool operator!=(index_t const& rhs) const { return value != rhs.value; }
    bool operator==(handle_t const& rhs) const { return value == rhs.idx.value; }
    bool operator!=(handle_t const& rhs) const { return value != rhs.idx.value; }

    explicit operator int() const { return value; }

    /// creates a handle from this idx and the given mesh
    handle_t of(grid_info const& g) const { return handle_t(g, index_t(value)); }
    handle_t of(grid const& g) const { return handle_t(g, index_t(value)); }

    /// indexes this primitive by a functor
    /// also works for attributes
    /// - e.g. v[position] or f[area]
    template <class FuncT, class ResultT = tmp::result_type_of<FuncT, index_t>>
    ResultT operator[](FuncT&& f) const;

    /// indexes this primitive by an attribute pointer
    /// if attr is null, returns writeable dummy location
    /// CAUTION: always returns same writeable dummy location, this is intended as a write-only value!
    template <class AttrT>
    AttrT& operator[](attribute<AttrT>* attr) const;
    /// indexes this primitive by an attribute pointer
    /// if attr is null, returns default constructed value
    template <class AttrT>
    AttrT const& operator[](attribute<AttrT> const* attr) const;
};

template <class tag>
struct primitive_handle
{
    template <class AttrT>
    using attribute = typename primitive<tag>::template attribute<AttrT>;
    using index_t = typename primitive<tag>::index;
    using handle_t = typename primitive<tag>::handle;

    grid_info info;
    index_t idx;

    primitive_handle() = default;
    primitive_handle(struct grid const& grid, index_t idx);
    primitive_handle(grid_info const& info, index_t idx) : info(info), idx(idx) {}

    bool operator==(index_t const& rhs) const { return idx == rhs; }
    bool operator!=(index_t const& rhs) const { return idx != rhs; }
    bool operator==(handle_t const& rhs) const { return info == rhs.info && idx == rhs.idx; }
    bool operator!=(handle_t const& rhs) const { return info != rhs.info || idx != rhs.idx; }

    explicit operator int() const { return (int)idx; }
    operator index_t() const { return idx; }

    struct grid grid() const;

    /// indexes this primitive by a functor
    /// also works for attributes
    /// - e.g. v[position] or f[area]
    template <class FuncT, class ResultT = tmp::result_type_of<FuncT, index_t>>
    ResultT operator[](FuncT&& f) const;

    /// indexes this primitive by an attribute pointer
    /// if attr is null, returns writeable dummy location
    template <class AttrT>
    AttrT& operator[](attribute<AttrT>* attr) const;
    /// indexes this primitive by an attribute pointer
    /// if attr is null, returns default constructed value
    template <class AttrT>
    AttrT const& operator[](attribute<AttrT> const* attr) const;

    bool is_valid() const { return idx.is_valid(); }    ///< valid idx
    bool is_invalid() const { return !idx.is_valid(); } ///< invalid idx
};

// ======================== INDICES ========================

struct cell_index : primitive_index<cell_tag>
{
    using primitive_index::primitive_index;
};
struct node_index : primitive_index<node_tag>
{
    using primitive_index::primitive_index;
};
struct face_index : primitive_index<face_tag>
{
    using primitive_index::primitive_index;
};
struct edge_index : primitive_index<edge_tag>
{
    using primitive_index::primitive_index;
};

// ======================== HANDLES ========================

struct cell_handle : primitive_handle<cell_tag>
{
    using primitive_handle::primitive_handle;

    bool is_boundary() const; ///< true if this cell lies at a boundary

    glm::ivec3 coords() const; ///< in [0..cells-1]
    glm::ivec3 pos_start() const { return coords(); }
    glm::ivec3 pos_end() const { return coords() + 1; }
    glm::vec3 center() const { return glm::vec3(pos_start() + pos_end()) * 0.5f; }

    node_handle node(int dx, int dy, int dz) const;

    std::array<node_handle, 8> nodes() const;  ///< all adjacent nodes
    std::array<face_handle, 6> faces() const;  ///< all adjacent faces
    std::array<edge_handle, 12> edges() const; ///< all adjacent edges
};

struct node_handle : primitive_handle<node_tag>
{
    using primitive_handle::primitive_handle;

    bool is_boundary() const; ///< true if this node is a boundary

    glm::ivec3 coords() const; ///< in [0..cells]

    edge_handle edge_pos(direction dir) const;
    edge_handle edge_neg(direction dir) const;
};

struct face_handle : primitive_handle<face_tag>
{
    using primitive_handle::primitive_handle;

    bool is_boundary() const; ///< true if this face lies at a boundary

    face_coords coords() const;
    direction normal() const { return coords().normal; }
    glm::ivec3 normal3() const { return ::grid::dir3(coords().normal); }
    glm::ivec3 pos_start() const { return coords().start; }
    glm::ivec3 pos_end() const { return coords().start + 1 - normal3(); }
    glm::vec3 center() const { return glm::vec3(pos_start() + pos_end()) * 0.5f; }

    cell_handle cellA() const; ///< lower-coordinate cell
    cell_handle cellB() const; ///< upper-coordinate cell

    node_handle node_start() const;

    std::array<cell_handle, 2> cells() const;
    std::array<node_handle, 4> nodes() const;
    std::array<edge_handle, 4> edges() const;
};

struct edge_handle : primitive_handle<edge_tag>
{
    using primitive_handle::primitive_handle;

    bool is_boundary() const; ///< true if this edge is a boundary

    edge_coords coords() const;
    direction dir() const { return coords().dir; }
    glm::ivec3 dir3() const { return ::grid::dir3(coords().dir); }
    glm::ivec3 pos_start() const { return coords().start; }
    glm::ivec3 pos_end() const { return coords().start + dir3(); }
    glm::vec3 center() const { return glm::vec3(pos_start() + pos_end()) * 0.5f; }

    node_handle nodeA() const; ///< lower-coordinate node
    node_handle nodeB() const; ///< upper-coordinate node

    std::array<node_handle, 2> nodes() const;
    std::array<cell_handle, 4> cells() const;
};

inline std::ostream& operator<<(std::ostream& out, node_handle const& h)
{
    auto c = h.coords();
    out << "node at [" << c.x << ", " << c.y << ", " << c.z << "]";
    return out;
}
inline std::ostream& operator<<(std::ostream& out, cell_handle const& h)
{
    auto c = h.coords();
    out << "cell from [" << c.x << ".." << c.x + 1 << ", " << c.y << ".." << c.y + 1 << ", " << c.z << ".." << c.z + 1 << "]";
    return out;
}
inline std::ostream& operator<<(std::ostream& out, edge_handle const& h)
{
    auto c = h.coords();
    auto s = c.start;
    switch (c.dir)
    {
    case DirX:
        out << "edge at [" << s.x << ".." << s.x + 1 << ", " << s.y << ", " << s.z << "]";
        break;
    case DirY:
        out << "edge at [" << s.x << ", " << s.y << ".." << s.y + 1 << ", " << s.z << "]";
        break;
    case DirZ:
        out << "edge at [" << s.x << ", " << s.y << ", " << s.z << ".." << s.z + 1 << "]";
        break;
    default:
        out << "edge at [" << s.x << ", " << s.y << ", " << s.z << ", <UNKNOWN DIR>]";
    }
    return out;
}
inline std::ostream& operator<<(std::ostream& out, face_handle const& h)
{
    auto c = h.coords();
    auto s = c.start;
    switch (c.normal)
    {
    case DirX:
        out << "face at [" << s.x << ", " << s.y << ".." << s.y + 1 << ", " << s.z << ".." << s.z + 1 << "]";
        break;
    case DirY:
        out << "face at [" << s.x << ".." << s.x + 1 << ", " << s.y << ", " << s.z << ".." << s.z + 1 << "]";
        break;
    case DirZ:
        out << "face at [" << s.x << ".." << s.x + 1 << ", " << s.y << ".." << s.y + 1 << ", " << s.z << "]";
        break;
    default:
        out << "face at [" << s.x << ", " << s.y << ", " << s.z << ", <UNKNOWN DIR>]";
    }
    return out;
}
}

// ======================== STD Extensions ========================

namespace std
{
template <class tag>
struct hash<grid::primitive_index<tag>>
{
    size_t operator()(grid::primitive_index<tag> const& i) const { return std::hash<int>()(i.value); }
};
template <>
struct hash<grid::face_index>
{
    size_t operator()(grid::face_index const& i) const { return std::hash<int>()(i.value); }
};
template <>
struct hash<grid::cell_index>
{
    size_t operator()(grid::cell_index const& i) const { return std::hash<int>()(i.value); }
};
template <>
struct hash<grid::edge_index>
{
    size_t operator()(grid::edge_index const& i) const { return std::hash<int>()(i.value); }
};
template <>
struct hash<grid::node_index>
{
    size_t operator()(grid::node_index const& i) const { return std::hash<int>()(i.value); }
};
}
