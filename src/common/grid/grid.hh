#pragma once

#include <glm/vec3.hpp>

#include "attributes.hh"
#include "cursors.hh"
#include "grid_info.hh"
#include "ranges.hh"

namespace grid
{
struct grid
{
    // members
private:
    grid_info _info;

    // properties
public:
    bool is_valid() const { return _info.is_valid(); }

    grid_info const& info() const { return _info; }

    // handles
public:
    cell_handle cell(glm::ivec3 coords) const { return {*this, cells().idxOf(coords)}; }
    node_handle node(glm::ivec3 coords) const { return {*this, nodes().idxOf(coords)}; }
    edge_handle edge(glm::ivec3 coords, direction dir) const { return {*this, edges().idxOf(coords, dir)}; }
    face_handle face(glm::ivec3 coords, direction dir) const { return {*this, faces().idxOf(coords, dir)}; }

    cell_handle cell(int x, int y, int z) const { return cell({x, y, z}); }
    node_handle node(int x, int y, int z) const { return node({x, y, z}); }
    edge_handle edge(int x, int y, int z, direction dir) const { return edge({x, y, z}, dir); }
    face_handle face(int x, int y, int z, direction dir) const { return face({x, y, z}, dir); }

    // ranges
public:
    cell_range cells() const { return cell_range(_info.cellCount()); }
    node_range nodes() const { return node_range(_info.cellCount() + 1); }
    edge_range edges() const { return edge_range(_info.cellCount() + 1); }
    face_range faces() const { return face_range(_info.cellCount()); }

    // ctor
public:
    grid() = default;
    explicit grid(glm::ivec3 cellCount) : _info{cellCount} {}
    grid(grid_info const& info) : _info(info) {}
};
}

/// ======== IMPLEMENTATIONS ========

#include "impl/impl_attributes.hh"
#include "impl/impl_cursors.hh"
#include "impl/impl_grid.hh"
#include "impl/impl_primitives.hh"
