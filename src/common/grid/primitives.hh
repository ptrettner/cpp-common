#pragma once

#include "fwd.hh"

namespace grid
{
template <typename tag>
struct primitive;

template <>
struct primitive<cell_tag>
{
    static constexpr auto name = "cell";

    using index = cell_index;
    using handle = cell_handle;

    using iterator = primitive_iterator<cell_tag>;
    using range = cell_range;

    template <class AttrT>
    using attribute = cell_attribute<AttrT>;

    static int size_of(grid const& grid);
};

template <>
struct primitive<face_tag>
{
    static constexpr auto name = "face";

    using index = face_index;
    using handle = face_handle;

    using iterator = primitive_iterator<face_tag>;
    using range = face_range;

    template <class AttrT>
    using attribute = face_attribute<AttrT>;

    static int size_of(grid const& grid);
};

template <>
struct primitive<edge_tag>
{
    static constexpr auto name = "edge";

    using index = edge_index;
    using handle = edge_handle;

    using iterator = primitive_iterator<edge_tag>;
    using range = edge_range;

    template <class AttrT>
    using attribute = edge_attribute<AttrT>;

    static int size_of(grid const& grid);
};

template <>
struct primitive<node_tag>
{
    static constexpr auto name = "node";

    using index = node_index;
    using handle = node_handle;

    using iterator = primitive_iterator<node_tag>;
    using range = node_range;

    template <class AttrT>
    using attribute = node_attribute<AttrT>;

    static int size_of(grid const& grid);
};
}
