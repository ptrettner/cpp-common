#pragma once

// https://helloacm.com/the-rdtsc-performance-timer-written-in-c/
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <iostream>

#include <common/macro_join.hh>

//  Windows
#ifdef _WIN32

#include <intrin.h>
inline uint64_t rdtsc()
{
    return __rdtsc();
}

//  Linux/GCC
#else

inline uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
    return ((uint64_t)hi << 32) | lo;
}

#endif

struct cyclecounter
{
    std::string name;
    int64_t loops;
    uint64_t start;
    cyclecounter(std::string const& n, int64_t l = 1) : name(n), loops(l) { start = rdtsc(); }
    ~cyclecounter()
    {
        auto c = rdtsc() - start;
        std::cout << "[" << name << "] " << c << " cycles";
        if (loops > 1)
            std::cout << " (" << c / (double)loops << " cycles/it)";
        std::cout << std::endl;
    }
};

#define CYCLES(...) cyclecounter MACRO_JOIN(_cycle_counter_, __LINE__)("" __VA_ARGS__)
