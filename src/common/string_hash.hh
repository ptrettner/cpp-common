#pragma once

#include <string>
#include <cstdint>
#include <vector>

inline uint32_t deterministicStringHash(const std::string& _str)
{
    uint64_t a = 6364136223846793005;
    uint64_t c = 1442695040888963407;
    std::vector<uint64_t> hashes(_str.size());
    for (unsigned int i = 0; i < _str.size(); ++i)
    {
        uint64_t x = _str[i] * (i + 1);
        unsigned int count = x % 5 + 1;
        for (unsigned int j = 0; j < count; j++)
        {
            x = a * x + c;
        }
        hashes[i] = x;
    }
    uint64_t hash = 0;
    for (auto it = begin(hashes); it != end(hashes); ++it)
    {
        hash ^= *it;
    }
    uint32_t reducedHash = static_cast<uint32_t>(hash ^ (hash >> 32));
    return reducedHash;
}

