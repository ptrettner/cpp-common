#pragma once

#include <algorithm>
#include <numeric>
#include <vector>

template <typename C>
auto minElement(C&& c) -> decltype(*begin(c))
{
    return *std::min_element(begin(c), end(c));
}
template <typename C>
auto maxElement(C&& c) -> decltype(*begin(c))
{
    return *std::max_element(begin(c), end(c));
}
template <typename C>
auto sum(C const& c) -> typename std::remove_reference<decltype(*c.begin())>::type
{
    return std::accumulate(c.begin() + 1, c.end(), *c.begin());
}
