#pragma once

/**
 * this header defines a 'THREADLOCAL' macro for thread-local variables in case the c++11 one is not yet supported
 */

#ifdef _MSC_VER

// Thread locality
#define THREADLOCAL __declspec(thread)

#else

#define THREADLOCAL __thread // GCC 4.7 has no thread_local yet

#endif
