#pragma once

#include <cstdio>
#include <iostream>

#include "breakpoint.hh"

// cond, msg, args
#define ASSERTF(cond, ...)                                             \
    do                                                                 \
    {                                                                  \
        if (!(cond))                                                   \
        {                                                              \
            std::cerr << "Assertion `" #cond "' failed." << std::endl; \
            fprintf(stderr, __VA_ARGS__);                              \
            std::cerr << std::endl;                                    \
            debug::breakpoint();                                       \
        }                                                              \
    } while (0)
