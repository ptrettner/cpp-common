#pragma once

/**
 * This header contains a helper function for demangling types
 */

#include <string>

namespace helper
{
std::string demangle(const char* name);
}
