#pragma once

#include "macro_join.hh"

/**
 * ScopeExit/Auto from
 * https://github.com/CppCon/CppCon2014/blob/master/Presentations/C%2B%2B11%20in%20the%20Wild%20-%20Techniques%20from%20a%20Real%20Codebase/auto.h
 * CppCon2014/Presentations/C++11 in the Wild - Techniques from a Real Codebase/auto.h
 *
 * Usage:
 *
 * { // logging is disabled for this scope and gets re-enabled at the end, even if return/continue/break is used inside
 *    SCOPE_ENTER(disableLogging());
 *    SCOPE_EXIT(enableLogging());
 *
 *    ...
 * }
 *
 */
namespace _internal
{
template <class Lambda>
class AtScopeExit
{
    Lambda& mLambda;

public:
    AtScopeExit(Lambda& action) : mLambda(action) {}
    ~AtScopeExit() { mLambda(); }
};
}

#define _internal_SCOPE_EXIT_1(lname, aname, ...) \
    auto lname = [&]()                            \
    {                                             \
        __VA_ARGS__;                              \
    };                                            \
    _internal::AtScopeExit<decltype(lname)> aname(lname);

#define _internal_SCOPE_EXIT_2(ctr, ...) \
    _internal_SCOPE_EXIT_1(MACRO_JOIN(Auto_func_, ctr), MACRO_JOIN(Auto_instance_, ctr), __VA_ARGS__)

#define SCOPE_EXIT(...) _internal_SCOPE_EXIT_2(__COUNTER__, __VA_ARGS__)
#define SCOPE_ENTER(...) __VA_ARGS__
