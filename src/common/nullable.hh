#pragma once

#include <cassert>
#include <cstddef>

template <typename T>
struct nullable
{
private:
    T mValue;
    bool mHasValue;

public:
    nullable(T const& v) : mValue(v), mHasValue(true) {}
    nullable(std::nullptr_t) : mHasValue(false) {}
    bool hasValue() const { return mHasValue; }
    T const& value() const
    {
        assert(mHasValue && "has no value");
        return mValue;
    }
    nullable& operator=(T const& v)
    {
        mValue = v;
        mHasValue = true;
        return *this;
    }
    nullable& operator=(std::nullptr_t)
    {
        mHasValue = false;
        return *this;
    }
};
