#include "random.hh"

#include "make_unique.hh"
#include "threadlocal.hh"
#include "string_hash.hh"
#include "hash_combine.hh"

#include <cstdlib>
#include <ctime>

#include <mutex>
#include <thread>

#include <glm/ext.hpp>

namespace
{
THREADLOCAL Random *sGlobalRandom = nullptr;
}

Random::Random(uint32_t _seed, bool _global) : mSeed(_seed), mMT(_seed), mGlobal(_global)
{
}

Random::Random() : mSeed((unsigned)clock()), mMT(mSeed), mGlobal(false)
{
}

Random::Random(uint32_t _seed) : mSeed(_seed), mMT(_seed), mGlobal(false)
{
}

Random::Random(const std::string &_seed) : mSeed(deterministicStringHash(_seed)), mMT(mSeed), mGlobal(false)
{
}

Random::~Random(void)
{
}

Random *Random::global()
{
    if (!sGlobalRandom)
    {
        static std::mutex m;
        m.lock();
        std::hash<std::thread::id> hasher;
        if (!sGlobalRandom)
            sGlobalRandom = new Random(hasher(std::this_thread::get_id()) + clock(), true);
        m.unlock();
    }
    return sGlobalRandom;
}

std::unique_ptr<Random> Random::getSubgenerator(uint32_t _seed)
{
    return std::make_unique<Random>(mSeed ^ _seed);
}

std::unique_ptr<Random> Random::getSubgenerator(const std::string &_seed)
{
    size_t seed = mSeed;
    common::hash_combine(seed, deterministicStringHash(_seed));
    return std::make_unique<Random>(seed);
}

int Random::uniformInt()
{
    return mMT();
}

int Random::uniformInt(int _inclusiveMin, int _inclusiveMax)
{
    double mapped = uniformDouble() * (_inclusiveMax - _inclusiveMin + 1) + _inclusiveMin;
    return static_cast<int>(glm::floor(mapped));
}

double Random::uniformDouble()
{
    return mMT() / (double)((uint64_t)1 << 32);
}

bool Random::coinFlip(double trueProb)
{
    return uniformDouble() < trueProb;
}

double Random::uniformDouble(double _min, double _max)
{
    return uniformDouble() * (_max - _min) + _min;
}

float Random::uniformFloat(float _min, float _max)
{
    return uniformDouble() * (_max - _min) + _min;
}

glm::vec2 Random::uniformVec2(glm::vec2 _min /*=glm::vec2(0)*/, glm::vec2 _max /*=glm::vec2(1)*/)
{
    return glm::vec2(uniformDouble(_min.x, _max.x), uniformDouble(_min.y, _max.y));
}

glm::vec3 Random::uniformVec3(glm::vec3 _min /*=glm::vec3(0)*/, glm::vec3 _max /*=glm::vec3(1)*/)
{
    return glm::vec3(uniformDouble(_min.x, _max.x), uniformDouble(_min.y, _max.y), uniformDouble(_min.z, _max.z));
}

glm::vec4 Random::uniformVec4(glm::vec4 _min /*=glm::vec4(0)*/, glm::vec4 _max /*=glm::vec4(1)*/)
{
    return glm::vec4(uniformDouble(_min.x, _max.x), uniformDouble(_min.y, _max.y), uniformDouble(_min.z, _max.z),
                     uniformDouble(_min.w, _max.w));
}

double Random::normalDouble()
{
    double q = 0;
    double u1 = 0;
    double u2 = 0;
    while (q == 0 || q >= 1)
    {
        u1 = uniformDouble(-1, 1);
        u2 = uniformDouble(-1, 1);
        q = u1 * u1 + u2 * u2;
    }
    double p = glm::sqrt(-2 * glm::log(q) / q);
    return u1 * p;
}

double Random::normalDouble(double _mean, double _deviation)
{
    return normalDouble() * _deviation + _mean;
}

unsigned int Random::poissonInt(double _lambda)
{
    double l = glm::exp(-_lambda);
    unsigned int k = 0;
    double p = 1;
    while (p >= l)
    {
        k++;
        p *= uniformDouble();
    }
    return k - 1;
}

double Random::exponentialDouble(double _lambda)
{
    return -glm::log(uniformDouble()) / _lambda;
}

glm::vec2 Random::randomDirection2()
{
    double angle = uniformDouble(0, glm::pi<double>() * 2);
    return glm::vec2(glm::sin(angle), glm::cos(angle));
}

glm::vec3 Random::randomDirection3()
{
    glm::vec3 vec(uniformDouble(-1, 1), uniformDouble(-1, 1), uniformDouble(-1, 1));
    while (glm::length(vec) > 1 || glm::length(vec) == 0)
    {
        vec = glm::vec3(uniformDouble(-1, 1), uniformDouble(-1, 1), uniformDouble(-1, 1));
    }
    return glm::normalize(vec);
}

glm::vec3 Random::randomBarycentric()
{
    // not uniformly distributed on a triangle
    /*float a, b;
    do
    {
       a = uniformDouble();
       b = uniformDouble();
    } while (a + b > 1);
    return glm::vec3(a, b, 1 - a - b);*/

    float a = uniformDouble();
    float b = uniformDouble();
    if (a + b > 1)
    {
        a = 1 - a;
        b = 1 - b;
    }
    return glm::vec3(a, b, 1 - a - b);
}

glm::vec3 Random::randomHemisphereDirection(const glm::vec3 &_normal)
{
    glm::vec3 randomVector = randomDirection3();
    if (glm::dot(randomVector, _normal) < 0)
    {
        randomVector *= -1.;
    }
    return randomVector;
}

