#pragma once

#include <gtest/gtest.h>

#define ASSERT_AXIS_UNIT_VECTOR(v, precision)                                                 \
    ASSERT_NEAR(glm::length((v)), 1.0f, precision);                                           \
    ASSERT_TRUE(glm::abs((v).x) < precision || glm::abs(glm::abs((v).x) - 1.0f) < precision); \
    ASSERT_TRUE(glm::abs((v).y) < precision || glm::abs(glm::abs((v).y) - 1.0f) < precision); \
    ASSERT_TRUE(glm::abs((v).z) < precision || glm::abs(glm::abs((v).z) - 1.0f) < precision)
