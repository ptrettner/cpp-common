#pragma once

/**
 * Enforces inline for a given function.
 * Causes compile errors if the function was not inlined
 *
 * Example:
 *
 * FORCEINLINE bool isOptimizedAllAir() const { return mMaterials == nullptr; }
 */

#ifndef _MSC_VER
#define FORCEINLINE __attribute__((always_inline))
#else
#define FORCEINLINE __forceinline
#endif
