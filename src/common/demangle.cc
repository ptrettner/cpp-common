#include "demangle.hh"

#ifdef __GNUG__
#include <cstdlib>
#include <memory>
#include <cxxabi.h>
#include <string>
#endif

// Type demangling
#ifdef __GNUG__

std::string helper::demangle(const char* name)
{
    if (!name)
        return "[NULL]";

    int status = -4; // some arbitrary value to eliminate the compiler warning

    // enable c++11 by passing the flag -std=c++11 to g++
    std::unique_ptr<char, void (*)(void*)> res{abi::__cxa_demangle(name, NULL, NULL, &status), std::free};

    return (status == 0) ? res.get() : name;
}

#else

// does nothing if not g++
std::string helper::demangle(const char* name)
{
    if (!name)
        return "[NULL]";

    return name;
}

#endif
