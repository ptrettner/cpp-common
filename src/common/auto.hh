#pragma once

// From Herb Sutter's https://www.youtube.com/watch?v=xnqTKD8uD64
// Using more auto is good. Having '_' as auto helps reduce clutter.
#define _ auto
