#pragma once

/**
 * This header defines __PRETTY_FUNCTION__ that returns the current function name
 * (non-msvc has builtin support for this)
 */

#ifdef _MSC_VER

// __PRETTY_FUNCTION__ is non-msvc
#define __PRETTY_FUNCTION__ __FUNCTION__

#endif
