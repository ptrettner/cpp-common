#pragma once

#define MACRO_JOIN_IMPL(arg1, arg2) arg1##arg2
#define MACRO_JOIN(arg1, arg2) MACRO_JOIN_IMPL(arg1, arg2)
