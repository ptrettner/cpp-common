#pragma once

#include <utility>

namespace detail
{
template <class ItT, class IndexT>
struct enumerated_iterator;

template <class BeginT, class EndT, class IndexT>
struct enumerated_range;
}

/// Usage:
/// for (auto [i, v] : enumerate(myRange))
///     ...
template <class RangeT>
auto enumerate(RangeT&& range) -> detail::enumerated_range<decltype(range.begin()), decltype(range.end()), int>;

// ========= IMPLEMENTATION ===========

namespace detail
{
template <class ItT, class IndexT>
struct enumerated_iterator
{
    ItT it;
    IndexT idx;

    auto operator*() -> decltype(std::make_pair(idx, *it)) { return std::make_pair(idx, *it); }
    auto operator*() const -> decltype(std::make_pair(idx, *it)) { return std::make_pair(idx, *it); }

    enumerated_iterator& operator++()
    {
        ++it;
        ++idx;
        return *this;
    }
    enumerated_iterator operator++(int)
    {
        auto i = *this;
        operator++();
        return i;
    }

    template <class ItT2, class IndexT2>
    bool operator==(enumerated_iterator<ItT2, IndexT2> const& rhs) const
    {
        return it == rhs.it;
    }
    template <class ItT2, class IndexT2>
    bool operator!=(enumerated_iterator<ItT2, IndexT2> const& rhs) const
    {
        return !operator==(rhs);
    }
};

template <class BeginT, class EndT, class IndexT>
struct enumerated_range
{
    BeginT _begin;
    EndT _end;

    enumerated_iterator<BeginT, IndexT> begin() const { return {_begin, 0}; }
    enumerated_iterator<EndT, IndexT> end() const { return {_end, -1}; }
};
}

template <class RangeT>
auto enumerate(RangeT&& range) -> detail::enumerated_range<decltype(range.begin()), decltype(range.end()), int>
{
    return {range.begin(), range.end()};
}
