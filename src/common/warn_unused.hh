#pragma once

#ifdef _MSC_VER
#define WARN_UNUSED
#else
#define WARN_UNUSED __attribute__((warn_unused_result))
#endif
